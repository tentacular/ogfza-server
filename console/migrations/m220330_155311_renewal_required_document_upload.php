<?php

use yii\db\Migration;

/**
 * Class m220330_155311_renewal_required_document_upload
 */
class m220330_155311_renewal_required_document_upload extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_required_document_upload', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'type_name' => $this->string()->notNull(),
            'file_path' => $this->string(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_required_document_upload');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_155311_renewal_required_document_upload cannot be reverted.\n";

        return false;
    }
    */
}
