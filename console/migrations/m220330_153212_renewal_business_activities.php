<?php

use yii\db\Migration;

/**
 * Class m220330_153212_renewal_business_activities
 */
class m220330_153212_renewal_business_activities extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_business_activities', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'activity' => $this->string()->notNull(),
            'approval_status' => $this->integer(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_business_activities');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_153212_renewal_business_activities cannot be reverted.\n";

        return false;
    }
    */
}
