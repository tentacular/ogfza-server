<?php

use yii\db\Migration;

/**
 * Class m220330_135202_details_of_registered_debenture
 */
class m220330_135202_details_of_registered_debenture extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_details_of_registered_debenture', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'details' => $this->string()->notNull(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_details_of_registered_debenture');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_135202_details_of_registered_debenture cannot be reverted.\n";

        return false;
    }
    */
}
