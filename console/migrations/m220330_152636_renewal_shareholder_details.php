<?php

use yii\db\Migration;

/**
 * Class m220330_152636_renewal_shareholder_details
 */
class m220330_152636_renewal_shareholder_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_shareholder_details', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'nationality' => $this->string()->notNull(),
            'number_of_shares' => $this->integer(),
            'ocupation' => $this->string()->notNull(),
            'resident' => $this->string(),
            'address' => $this->string()->notNull(),
            'type' => $this->integer(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_shareholder_details');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_152636_renewal_shareholder_details cannot be reverted.\n";

        return false;
    }
    */
}
