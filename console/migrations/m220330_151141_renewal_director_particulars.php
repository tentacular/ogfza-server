<?php

use yii\db\Migration;

/**
 * Class m220330_151141_renewal_director_particulars
 */
class m220330_151141_renewal_director_particulars extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_director_particulars', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'dob' => $this->string()->notNull(),
            'ocupation' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'nationality' => $this->string()->notNull(),
            'file_path' => $this->string(),
            'date_added' => $this->dateTime(),
            'type' => $this->integer(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_director_particulars');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_151141_renewal_director_particulars cannot be reverted.\n";

        return false;
    }
    */
}
