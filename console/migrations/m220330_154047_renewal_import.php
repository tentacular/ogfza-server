<?php

use yii\db\Migration;

/**
 * Class m220330_154047_renewal_import
 */
class m220330_154047_renewal_import extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_import', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'nature_of_import' => $this->string()->notNull(),
            'volume' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
            'volum_ng' => $this->string(),
            'value_ng' => $this->string(),
            'volum_oversea' => $this->string(),
            'value_oversea' => $this->string(),
            'comment' => $this->string()->notNull(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_import');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_154047_renewal_import cannot be reverted.\n";

        return false;
    }
    */
}
