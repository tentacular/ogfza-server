<?php

use yii\db\Migration;

/**
 * Class m220330_161756_renewal_oath
 */
class m220330_161756_renewal_oaths extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_oaths', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'designation' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'oath_date' => $this->string()->notNull(),
            'director_signature' => $this->string()->notNull(),
            'secretary_signature' => $this->string()->notNull(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_oaths');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_161756_renewal_oath cannot be reverted.\n";

        return false;
    }
    */
}
