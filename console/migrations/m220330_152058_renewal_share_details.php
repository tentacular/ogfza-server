<?php

use yii\db\Migration;

/**
 * Class m220330_152058_renewal_share_details
 */
class m220330_152058_renewal_share_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_share_details', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'nameauthorised_shares_capital' => $this->integer()->notNull(),
            'dividend_into' => $this->integer()->notNull(),
            'dividend_ratio' => $this->integer()->notNull(),
            'issued_share_capital' => $this->integer()->notNull(),
            'paid_up_capital' => $this->string()->notNull(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_share_details');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_152058_renewal_share_details cannot be reverted.\n";

        return false;
    }
    */
}
