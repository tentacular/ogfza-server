<?php

use yii\db\Migration;

/**
 * Class m220330_153550_renewal_vendor_service
 */
class m220330_153550_renewal_vendor_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_vendor_service', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'service' => $this->string()->notNull(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_vendor_service');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_153550_renewal_vendor_service cannot be reverted.\n";

        return false;
    }
    */
}
