<?php

use yii\db\Migration;

/**
 * Class m210618_160023_init_rbac
 */
class m210618_160023_init_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        // add "screen" permission
        $screen = $auth->createPermission('screen');
        $screen->description = 'To be able to screen applicant';
        $auth->add($screen);
		
		// add "screen verify" permission
        $screenverify = $auth->createPermission('screenverify');
        $screenverify->description = 'To be able to verify applicant details';
        $auth->add($screenverify);
		
		// add "spv" permission
        $spv = $auth->createPermission('spvp');
        $spv->description = 'To be able to do verify applicant by spv';
        $auth->add($spv);
		
		// add "legal" permission
        $legal = $auth->createPermission('legalp');
        $legal->description = 'To be able to do verify applicant by legal';
        $auth->add($legal);
		
		// add "ceo" permission
        $ceo = $auth->createPermission('ceop');
        $ceo->description = 'To be able to do verify applicant by ceo';
        $auth->add($ceo);
		
		// add "account" permission
        $account = $auth->createPermission('accountp');
        $account->description = 'To be able to do verify applicant by account';
        $auth->add($account);


        // add "generalr" role and give this role the "createPost" permission
        $general = $auth->createRole('general');
        $auth->add($general);
        $auth->addChild($general, $screen);
        $auth->addChild($general, $screenverify);
		
		// add "generalr" role and give this role the "createPost" permission
        $spvrole = $auth->createRole('spv');
        $auth->add($spvrole);
        $auth->addChild($spvrole, $screen);
        $auth->addChild($spvrole, $screenverify);
        $auth->addChild($spvrole, $spv);
		
		$legalrole = $auth->createRole('legal');
        $auth->add($legalrole);
        $auth->addChild($legalrole, $legal);
		
		$ceorole = $auth->createRole('ceo');
        $auth->add($ceorole);
        $auth->addChild($ceorole, $ceo);
		
		$accountrole = $auth->createRole('account');
        $auth->add($accountrole);
        $auth->addChild($accountrole, $account);


        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($spvrole, 1);
        
    }
    
    public function down()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
    }
}
