<?php

use yii\db\Migration;

/**
 * Class m220520_170832_country
 */
class m220520_170832_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'sortname' => $this->string()->null(),
            'name' => $this->string()->null(),
            'phonecode' => $this->integer()->null(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('country');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220520_170832_country cannot be reverted.\n";

        return false;
    }
    */
}
