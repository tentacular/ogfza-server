<?php

use yii\db\Migration;

/**
 * Class m220330_154652_renewal_expor
 */
class m220330_154652_renewal_export extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_export', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'nature_of_export' => $this->string()->notNull(),
            'volume' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_export');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_154652_renewal_expor cannot be reverted.\n";

        return false;
    }
    */
}
