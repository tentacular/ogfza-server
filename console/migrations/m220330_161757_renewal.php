<?php

use yii\db\Migration;

/**
 * Class m220330_161756_renewal_oath
 */
class m220330_161757_renewal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'application_stage' => $this->integer(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
            'root_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_161756_renewal_oath cannot be reverted.\n";

        return false;
    }
    */
}
