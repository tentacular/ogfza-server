<?php

use yii\db\Migration;

/**
 * Class m220330_155837_renewal_foreign_direct_investment
 */
class m220330_155837_renewal_foreign_direct_investment_main extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_foreign_direct_investment', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'volume' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
            'others' => $this->string()->notNull(),
            'file_path' => $this->string(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_foreign_direct_investment');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_155837_renewal_foreign_direct_investment cannot be reverted.\n";

        return false;
    }
    */
}
