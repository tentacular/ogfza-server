<?php

use yii\db\Migration;

/**
 * Class m220330_161054_renewal_space_size
 */
class m220330_161054_renewal_space_size extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('renewal_space_size', [
            'id' => $this->primaryKey(),
            'renewal_id' => $this->integer(),
            'stacking_area' => $this->string(),
            'warehouse' => $this->string(),
            'office_space' => $this->string(),
            'comment' => $this->string(),
            'date_created' => $this->dateTime(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('renewal_space_size');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220330_161054_renewal_space_size cannot be reverted.\n";

        return false;
    }
    */
}
