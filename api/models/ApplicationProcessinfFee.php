<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "application_processinf_fee".
 *
 * @property int $id
 * @property int|null $root_id
 * @property string|null $file_path
 * @property string $date_created
 * @property int $status
 */
class ApplicationProcessinfFee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'application_processinf_fee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root_id', 'status'], 'integer'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }


}
