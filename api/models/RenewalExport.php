<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_export".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $nature_of_export
 * @property string $volume
 * @property string $value
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalExport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_export';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['nature_of_export', 'volume', 'value'], 'required'],
            [['date_created'], 'safe'],
            [['nature_of_export', 'volume', 'value', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'nature_of_export' => 'Nature Of Export',
            'volume' => 'Volume',
            'value' => 'Value',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
