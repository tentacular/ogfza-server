<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_company_contact_details".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string|null $address
 * @property string|null $website
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $comment
 * @property string $date_created
 * @property int $status
 */

class RenewalCompanyContactDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_company_contact_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['comment'], 'string'],
            [['date_created'], 'safe'],
            [['address'], 'string', 'max' => 200],
            [['website', 'email'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'address' => 'Address',
            'website' => 'Website',
            'email' => 'Email',
            'phone' => 'Phone',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
