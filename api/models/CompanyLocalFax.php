<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "company_local_fax".
 *
 * @property int $id
 * @property int $root_id
 * @property string $value
 * @property int $status
 * @property int $approved_by_id
 */
class CompanyLocalFax extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_local_fax';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root_id', 'value', 'status', 'approved_by_id'], 'required'],
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['value'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'value' => 'Value',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
