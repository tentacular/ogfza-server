<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_company_info_incoporation".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $date_created
 * @property int $status
 */
class RenewalCompanyInfoIncoporation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_company_info_incoporation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['value'], 'string'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
