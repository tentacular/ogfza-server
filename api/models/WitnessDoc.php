<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "witness_doc".
 *
 * @property int $id
 * @property int $root_id
 * @property string $file_path
 * @property int $status
 * @property int $approved_by_id
 */
class WitnessDoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'witness_doc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['file_path'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'file_path' => 'File Path',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
