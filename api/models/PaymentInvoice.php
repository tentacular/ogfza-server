<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "payment_invoice".
 *
 * @property int $id
 * @property int $root_id
 * @property string $amount
 * @property string $rrr
 * @property int $status
 * @property int $approved_by_id
 */
class PaymentInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['amount', 'rrr'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'amount' => 'Amount',
            'rrr' => 'Rrr',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
