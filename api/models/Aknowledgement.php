<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "aknowledgement".
 *
 * @property int $id
 * @property int $root_id
 * @property string $name
 * @property string $designation
 * @property string $signature
 * @property string $date_created
 * @property string $company_stamp
 * @property int $status
 * @property int $approved_by_id
 */
class Aknowledgement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aknowledgement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            s
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['date_created'], 'safe'],
            [['name', 'designation', 'signature', 'company_stamp'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'name' => 'Name',
            'designation' => 'Designation',
            'signature' => 'Signature',
            'date_created' => 'Date Created',
            'company_stamp' => 'Company Stamp',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
