<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "nature_of_business_operation".
 *
 * @property int $id
 * @property int $root_id
 * @property string $description
 * @property int $status
 * @property int $approved_by_id
 */
class natureOfBusinessOperation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nature_of_business_operation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'description' => 'Description',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
