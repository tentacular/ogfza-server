<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "contact_local_name".
 *
 * @property int $id
 * @property int $root_id
 * @property string $title
 * @property string $value
 * @property int $status
 * @property int $approved_by_id
 */
class ContactLocalName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_local_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root_id', 'title', 'value', 'status', 'approved_by_id'], 'required'],
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['title'], 'string', 'max' => 10],
            [['value'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'title' => 'Title',
            'value' => 'Value',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
