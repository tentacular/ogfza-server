<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_incoporation_type".
 *
 * @property int|null $id
 * @property int|null $renewal_id
 * @property string|null $incoporation_name
 * @property string $date_created
 * @property int $status
 */
class RenewalIncoporationType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_incoporation_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'renewal_id', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['status'], 'required'],
            [['incoporation_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'incoporation_name' => 'Incoporation Name',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
