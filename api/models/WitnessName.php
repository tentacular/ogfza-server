<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "witness_name".
 *
 * @property int $cid
 * @property int $root_id
 * @property string $value
 * @property int $status
 * @property int $approved_by_id
 */
class WitnessName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'witness_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['value'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cid' => 'Cid',
            'root_id' => 'Root ID',
            'value' => 'Value',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
