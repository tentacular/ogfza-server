<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_oaths".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $name
 * @property string $designation
 * @property string $phone
 * @property string $oath_date
 * @property string $director_signature
 * @property string $secretary_signature
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalOaths extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $imageFileDirectorSig;
    public $imageFileSecretarySig;
    public static function tableName()
    {
        return 'renewal_oaths';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['name', 'designation', 'phone', 'oath_date', 'director_signature', 'secretary_signature'], 'required'],
            [['date_created'], 'safe'],
            [['name', 'designation', 'phone', 'oath_date', 'director_signature', 'secretary_signature', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'name' => 'Name',
            'designation' => 'Designation',
            'phone' => 'Phone',
            'oath_date' => 'Oath Date',
            'director_signature' => 'Director Signature',
            'secretary_signature' => 'Secretary Signature',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }

    public function upload()
    {
		
        if ($this->validate()) {
			if($this->isNewRecord){
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewNameDirect = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFileDirectorSig->baseName))) . $newRand.'.' . $this->imageFile->imageFileDirectorSig;
			
				$filePath = $rootPath .$imageNewNameDirect ;
				$fileOriginPath = 'images/'.$imageNewNameDirect;

                $imageNewNameSec = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFileSecretarySig->baseName))) . $newRand.'.' . $this->imageFileSecretarySig->extension;
			
				$filePath2 = $rootPath .$imageNewNameSec ;
				$fileOriginPath2 = 'images/'.$imageNewNameSec;

				$this->director_signature  = $fileOriginPath;
                $this->secretary_signature  = $fileOriginPath2;
                $this->imageFileDirectorSig->saveAs($filePath);
                $this->imageFileSecretarySig->saveAs($filePath2);
				$this->save();
				
			}else{
				if(empty($this->imageFileSecretarySig && $this->imageFileDirectorSig)){
					$this->save();
				}else{
                    $newRand = rand(1,1000000000001010);
                    $rootPath = '@api/web/uploads/images/';
                    $imageNewNameDirect = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFileDirectorSig->baseName))) . $newRand.'.' . $this->imageFile->imageFileDirectorSig;
                
                    $filePath = $rootPath .$imageNewNameDirect ;
                    $fileOriginPath = 'images/'.$imageNewNameDirect;
    
                    $imageNewNameSec = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFileSecretarySig->baseName))) . $newRand.'.' . $this->imageFileSecretarySig->extension;
                
                    $filePath2 = $rootPath .$imageNewNameSec ;
                    $fileOriginPath2 = 'images/'.$imageNewNameSec;
    
                    $this->director_signature  = $fileOriginPath;
                    $this->secretary_signature  = $fileOriginPath2;
                    $this->imageFileDirectorSig->saveAs($filePath);
                    $this->imageFileSecretarySig->saveAs($filePath2);
                    $this->save();
				}
			}
			
            return true;
        } else {
            return false;
        }
    }
}
