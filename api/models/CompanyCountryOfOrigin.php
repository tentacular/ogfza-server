<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "company_country_of_origin".
 *
 * @property int $id
 * @property int $root_id
 * @property int $value
 * @property int $status
 * @property int $approved_by_id
 */
class CompanyCountryOfOrigin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_country_of_origin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id',  'status', 'approved_by_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'value' => 'Value',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
