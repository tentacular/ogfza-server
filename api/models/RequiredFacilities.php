<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "required_facilities".
 *
 * @property int $id
 * @property int $root_id
 * @property int $type
 * @property int $quantity
 * @property int $quantity_measurement
 * @property int $status
 * @property int $approved_by_id
 */
class RequiredFacilities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'required_facilities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root_id',  'status', 'approved_by_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'type' => 'Type',
            'quantity' => 'Quantity',
            'quantity_measurement' => 'Quantity Measurement',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
