<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_foreign_direct_investment".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $volume
 * @property string $value
 * @property string $others
 * @property string|null $file_path
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalForeignDirectInvestment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $imageFile;
    public static function tableName()
    {
        return 'renewal_foreign_direct_investment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['volume', 'value', 'others'], 'required'],
            [['date_created'], 'safe'],
            [['volume', 'value', 'others', 'file_path', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'volume' => 'Volume',
            'value' => 'Value',
            'others' => 'Others',
            'file_path' => 'File Path',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }

    public function upload()
    {
		
        if ($this->validate()) {
			if($this->isNewRecord){
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewName = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFile->baseName))) . $newRand.'.' . $this->imageFile->extension;
			
				$filePath = $rootPath .$imageNewName ;
				$fileOriginPath = 'images/'.$imageNewName;
				$this->file_path  = $fileOriginPath;
				$this->save();
				$this->imageFile->saveAs($filePath);
			}else{
				if(empty($this->imageFile)){
					$this->save();
				}else{
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewName = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFile->baseName))) . $newRand.'.' . $this->imageFile->extension;
			
				$filePath = $rootPath .$imageNewName ;
				$fileOriginPath = 'images/'.$imageNewName;
				$this->file_path  = $fileOriginPath;
				$this->save();
				$this->imageFile->saveAs($filePath);
				}
			}
			
            return true;
        } else {
            return false;
        }
    }
}
