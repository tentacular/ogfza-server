<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "country_in_operation".
 *
 * @property int $id
 * @property int $root_id
 * @property int $country_id
 * @property int $status
 * @property int $approved_by_id
 */
class CountryInOperation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country_in_operation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['country_id'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'country_id' => 'Country ID',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
