<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_details_of_registered_debenture".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $details
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalDetailsOfRegisteredDebenture extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_details_of_registered_debenture';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['details'], 'required'],
            [['date_created'], 'safe'],
            [['details', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'details' => 'Details',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
