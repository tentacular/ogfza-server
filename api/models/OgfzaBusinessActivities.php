<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "ogfza_business_activities".
 *
 * @property int $id
 * @property int $root_id
 * @property string $description
 * @property int $status
 * @property int $approved_by_id
 */
class OgfzaBusinessActivities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ogfza_business_activities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'description' => 'Description',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
