<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "expected_start_date".
 *
 * @property int $id
 * @property int $root_id
 * @property string $start_date
 * @property int $status
 * @property int $approved_by_id
 */
class ExpectedStartDate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'expected_start_date';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['start_date'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'start_date' => 'Start Date',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
