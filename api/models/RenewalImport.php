<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_import".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $nature_of_import
 * @property string $volume
 * @property string $value
 * @property string|null $volum_ng
 * @property string|null $value_ng
 * @property string|null $volum_oversea
 * @property string|null $value_oversea
 * @property string $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalImport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_import';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['nature_of_import', 'volume', 'value', 'comment'], 'required'],
            [['date_created'], 'safe'],
            [['nature_of_import', 'volume', 'value', 'volum_ng', 'value_ng', 'volum_oversea', 'value_oversea', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'nature_of_import' => 'Nature Of Import',
            'volume' => 'Volume',
            'value' => 'Value',
            'volum_ng' => 'Volum Ng',
            'value_ng' => 'Value Ng',
            'volum_oversea' => 'Volum Oversea',
            'value_oversea' => 'Value Oversea',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
