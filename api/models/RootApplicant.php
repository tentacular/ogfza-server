<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "root_applicant".
 *
 * @property int $id
 * @property string $email
 * @property int $application_stage
 * @property int $approval_stage
 * @property int $status
 */
class RootApplicant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'root_applicant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['email', 'application_stage', 'approval_stage', 'status'], 'required'],
            [['application_stage', 'approval_stage', 'status','approval_stage_countter','processing_fee_status'], 'integer'],
            [['email'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'application_stage' => 'Application Stage',
            'approval_stage' => 'Approval Stage',
            'approval_stage_countter' => 'Approval Stage Countter',
            'status' => 'Status',
        ];
    }
	
	public function getCompanycountryofregistration(){
		return $this->hasOne(CompanyCountryOfOrigin::className(), ['root_id' => 'id']);
	}
	
	public function getCompanyaddress(){
		return $this->hasOne(CompanyAddress::className(), ['root_id' => 'id']);
	}
	
	public function getCompanyfax(){
		return $this->hasOne(CompanyFax::className(), ['root_id' => 'id']);
	}
	
	public function getCompanylocaladdress(){
		return $this->hasOne(CompanyLocalAddress::className(), ['root_id' => 'id']);
	}
	
	public function getCompanylocalfax(){
		return $this->hasOne(CompanyLocalFax::className(), ['root_id' => 'id']);
	}
	
	public function getCompanylocaltel(){
		return $this->hasOne(CompanyLocalTel::className(), ['root_id' => 'id']);
	}
	
	public function getCompanylocalwebsite(){
		return $this->hasOne(CompanyLocalWebsite::className(), ['root_id' => 'id']);
	}
	
	public function getCompanyname(){
		return $this->hasOne(CompanyName::className(), ['root_id' => 'id']);
	}
	
	public function getCompanytel(){
		return $this->hasOne(CompanyTel::className(), ['root_id' => 'id']);
	}
	
	public function getCompanyunderstanding(){
		return $this->hasOne(CompanyUnderstanding::className(), ['root_id' => 'id']);
	}
	
	public function getCompanywebsite(){
		return $this->hasOne(CompanyWebsite::className(), ['root_id' => 'id']);
	}
	
	public function getContactlocalname(){
		return $this->hasOne(ContactLocalName::className(), ['root_id' => 'id']);
	}
	
	public function getContactname(){
		return $this->hasOne(ContactName::className(), ['root_id' => 'id']);
	}
	
	public function getCountryinoperation(){
		return $this->hasMany(CountryInOperation::className(), ['root_id' => 'id']);
	}
	
	public function getDescriptionofactivity(){
		return $this->hasOne(DescriptionOfActivity::className(), ['root_id' => 'id']);
	}
	
	public function getDocumentupload(){
		return $this->hasMany(DocumentUpload::className(), ['root_id' => 'id']);
	}
	
	public function getExpectedstartdate(){
		return $this->hasOne(ExpectedStartDate::className(), ['root_id' => 'id']);
	}
	
	public function getExportmovement(){
		return $this->hasMany(ExportMovement::className(), ['root_id' => 'id']);
	}
	
	public function getImportmovement(){
		return $this->hasMany(ImportMovement::className(), ['root_id' => 'id']);
	}
	
	public function getMouapprovaldate(){
		return $this->hasOne(MouApprovalDate::className(), ['root_id' => 'id']);
	}
	
	public function getMounumberofshares(){
		return $this->hasOne(MouNumberOfShares::className(), ['root_id' => 'id']);
	}
	
	public function getMoureglocation(){
		return $this->hasOne(MouRegLocation::className(), ['root_id' => 'id']);
	}
	
	public function getNatureofbusinessoperation(){
		return $this->hasOne(natureOfBusinessOperation::className(), ['root_id' => 'id']);
	}
	
	public function getObjofestablishment(){
		return $this->hasOne(ObjOfEstablishment::className(), ['root_id' => 'id']);
	}
	
	public function getOgfzabusinessactivities(){
		return $this->hasOne(OgfzaBusinessActivities::className(), ['root_id' => 'id']);
	}
	
	public function getPaymentinvoice(){
		return $this->hasOne(PaymentInvoice::className(), ['root_id' => 'id']);
	}
	
	public function getReginnigeria(){
		return $this->hasOne(RegInNigeria::className(), ['root_id' => 'id']);
	}
	
	public function getRequiredfacilities(){
		return $this->hasMany(RequiredFacilities::className(), ['root_id' => 'id']);
	}
	
	public function getShareholders(){
		return $this->hasMany(ShareHolders::className(), ['root_id' => 'id']);
	}
	
	public function getShares(){
		return $this->hasOne(Shares::className(), ['root_id' => 'id']);
	}
	
	public function getWitnessaddress(){
		return $this->hasOne(WitnessAddress::className(), ['root_id' => 'id']);
	}
	
	public function getWitnessdoc(){
		return $this->hasOne(WitnessDoc::className(), ['root_id' => 'id']);
	}
	
	public function getWitnessname(){
		return $this->hasOne(WitnessName::className(), ['root_id' => 'id']);
	}
	
	public function getWitnessoccupation(){
		return $this->hasOne(WitnessOccupation::className(), ['root_id' => 'id']);
	}
	
	public function getWitnesssignature(){
		return $this->hasOne(WitnessSignature::className(), ['root_id' => 'id']);
	}
	public function getRenewal(){
		return $this->hasMany(WitnessSignature::className(), ['root_id' => 'id']);
	}
}
