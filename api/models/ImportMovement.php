<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "import_movement".
 *
 * @property int $id
 * @property int $root_id
 * @property int $route_type
 * @property string $cargo_type
 * @property int $tons
 * @property int $container
 * @property int $value
 * @property int $status
 * @property int $approved_by_id
 */
class ImportMovement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'import_movement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [[ 'route_type', 'tons', 'container', 'value', 'status', 'approved_by_id'], 'string'],
            [['cargo_type'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'route_type' => 'Route Type',
            'cargo_type' => 'Cargo Type',
            'tons' => 'Tons',
            'container' => 'Container',
            'value' => 'Value',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
