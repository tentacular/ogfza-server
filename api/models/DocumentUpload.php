<?php

namespace api\models;

use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "document_upload".
 *
 * @property int $id
 * @property int $root_id
 * @property int $type
 * @property string $parth
 * @property int $status
 * @property int $approved_by_id
 */
class DocumentUpload extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
	public $imageFile;
    public static function tableName()
    {
        return 'document_upload';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           
            [['root_id',  'status', 'approved_by_id'], 'integer'],
            [['parth'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'type' => 'Type',
            'parth' => 'Parth',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
	
	public function upload()
    {
		
        if ($this->validate()) {
			if($this->isNewRecord){
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewName = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFile->baseName))) . $newRand.'.' . $this->imageFile->extension;
			
				$filePath = $rootPath .$imageNewName ;
				$fileOriginPath = 'images/'.$imageNewName;
				$this->parth  = $fileOriginPath;
				$this->save();
				$this->imageFile->saveAs($filePath);
			}else{
				if(empty($this->imageFile)){
					$this->save();
				}else{
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewName = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFile->baseName))) . $newRand.'.' . $this->imageFile->extension;
			
				$filePath = $rootPath .$imageNewName ;
				$fileOriginPath = 'images/'.$imageNewName;
				$this->parth  = $fileOriginPath;
				$this->save();
				$this->imageFile->saveAs($filePath);
				}
			}
			
            return true;
        } else {
            return false;
        }
    }
}
