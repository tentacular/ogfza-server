<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal".
 *
 * @property int $id
 * @property string $email
 * @property int|null $application_stage
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class Renewal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['application_stage', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['email', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'application_stage' => 'Application Stage',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }

  public function getBusinessactivity(){
		return $this->hasOne(RenewalBusinessActivities::className(), ['renewal_id' => 'id']);
	}

  public function getCompanyContactDetails(){
		return $this->hasOne(RenewalCompanyContactDetails::className(), ['renewal_id' => 'id']);
	
  }
  public function getCompanyContactPerson(){
		return $this->hasMany(RenewalCompanycontactPerson::className(), ['renewal_id' => 'id']);
	}

  public function getCompanyInfo(){
		return $this->hasOne(RenewalCompanyInfo::className(), ['renewal_id' => 'id']);
	}

  public function getCompanyInfoIncoporation(){
		return $this->hasMany(RenewalCompanyInfoIncoporation::className(), ['renewal_id' => 'id']);
	}

  public function getRegisteredDebenture(){
		return $this->hasOne(RenewalDetailsOfRegisteredDebenture::className(), ['renewal_id' => 'id']);
	}

  public function getDirectorParticulars(){
		return $this->hasMany(RenewalDirectorParticulars::className(), ['renewal_id' => 'id']);
	}

  public function getExport(){
		return $this->hasMany(RenewalExport::className(), ['renewal_id' => 'id']);
	}

  public function getForeignDirectInvestment(){
		return $this->hasMany(RenewalForeignDirectInvestment::className(), ['renewal_id' => 'id']);
	}

  public function getImport(){
		return $this->hasMany(RenewalImport::className(), ['renewal_id' => 'id']);
	}

  public function getIncoporationType(){
		return $this->hasMany(RenewalIncoporationType::className(), ['renewal_id' => 'id']);
	}

  public function getOaths(){
		return $this->hasOne(RenewalOaths::className(), ['renewal_id' => 'id']);
	}

  public function getDocumentUpload(){
		return $this->hasMany(RenewalRequiredDocumentUpload::className(), ['renewal_id' => 'id']);
	}
  public function getShareDetails(){
		return $this->hasOne(RenewalShareDetails::className(), ['renewal_id' => 'id']);
	}

  public function getShareHoldersDetails(){
		return $this->hasMany(RenewalShareholderDetails::className(), ['renewal_id' => 'id']);
	}

  public function getSpaceSize(){
		return $this->hasOne(RenewalSpaceSize::className(), ['renewal_id' => 'id']);
	}

  public function getVendorService(){
		return $this->hasMany(RenewalVendorService::className(), ['renewal_id' => 'id']);
	}

  public function getRootApplicant(){
		return $this->hasOne(RootApplicant::className(), ['id' => 'root_id']);
	}

}
