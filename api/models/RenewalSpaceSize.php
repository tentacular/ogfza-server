<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_space_size".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string|null $stacking_area
 * @property string|null $warehouse
 * @property string|null $office_space
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalSpaceSize extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_space_size';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['stacking_area', 'warehouse', 'office_space', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'stacking_area' => 'Stacking Area',
            'warehouse' => 'Warehouse',
            'office_space' => 'Office Space',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
