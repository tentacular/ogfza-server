<?php

namespace api\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "renewal_director_particulars".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $name
 * @property string $dob
 * @property string $ocupation
 * @property string $address
 * @property string $nationality
 * @property string|null $file_path
 * @property string|null $date_added
 * @property int|null $type
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalDirectorParticulars extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $imageFile;
    public static function tableName()
    {
        return 'renewal_director_particulars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'type', 'status'], 'integer'],
            [['name', 'dob', 'ocupation', 'address', 'nationality'], 'required'],
            [['date_added', 'date_created'], 'safe'],
            [['name', 'dob', 'ocupation', 'address', 'nationality', 'file_path', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'name' => 'Name',
            'dob' => 'Dob',
            'ocupation' => 'Ocupation',
            'address' => 'Address',
            'nationality' => 'Nationality',
            'file_path' => 'File Path',
            'date_added' => 'Date Added',
            'type' => 'Type',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }

    public function upload()
    {
		
        if ($this->validate()) {
			if($this->isNewRecord){
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewName = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFile->baseName))) . $newRand.'.' . $this->imageFile->extension;
			
				$filePath = $rootPath .$imageNewName ;
				$fileOriginPath = 'images/'.$imageNewName;
				$this->file_path  = $fileOriginPath;
				$this->save();
				$this->imageFile->saveAs($filePath);
			}else{
				if(empty($this->imageFile)){
					$this->save();
				}else{
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewName = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFile->baseName))) . $newRand.'.' . $this->imageFile->extension;
			
				$filePath = $rootPath .$imageNewName ;
				$fileOriginPath = 'images/'.$imageNewName;
				$this->file_path  = $fileOriginPath;
				$this->save();
				$this->imageFile->saveAs($filePath);
				}
			}
			
            return true;
        } else {
            return false;
        }
    }
}
