<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_companycontact_person".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $phone
 * @property string $date_created
 * @property int $status
 */
class RenewalCompanycontactPerson extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_companycontact_person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['name', 'email'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
