<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "doc_upload".
 *
 * @property int $id
 * @property int $root_id
 * @property string $document_type
 * @property string $file_path
 * @property int $status
 * @property int $approved_by_id
 */
class DocUpload extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doc_upload';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['document_type', 'file_path'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'document_type' => 'Document Type',
            'file_path' => 'File Path',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
}
