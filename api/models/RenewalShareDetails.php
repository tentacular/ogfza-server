<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_share_details".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property int $nameauthorised_shares_capital
 * @property int $dividend_into
 * @property int $dividend_ratio
 * @property int $issued_share_capital
 * @property string $paid_up_capital
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalShareDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_share_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'nameauthorised_shares_capital', 'dividend_into', 'dividend_ratio', 'issued_share_capital', 'status'], 'integer'],
            [['nameauthorised_shares_capital', 'dividend_into', 'dividend_ratio', 'issued_share_capital', 'paid_up_capital'], 'required'],
            [['date_created'], 'safe'],
            [['paid_up_capital', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'nameauthorised_shares_capital' => 'Nameauthorised Shares Capital',
            'dividend_into' => 'Dividend Into',
            'dividend_ratio' => 'Dividend Ratio',
            'issued_share_capital' => 'Issued Share Capital',
            'paid_up_capital' => 'Paid Up Capital',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
