<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_business_activities".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $activity
 * @property int|null $approval_status
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalBusinessActivities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_business_activities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'approval_status', 'status'], 'integer'],
            [['activity'], 'required'],
            [['date_created'], 'safe'],
            [['activity', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'activity' => 'Activity',
            'approval_status' => 'Approval Status',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
