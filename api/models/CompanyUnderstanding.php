<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "company_understanding".
 *
 * @property int $id
 * @property int $root_id
 * @property string $name
 * @property string $designation
 * @property string $signature
 * @property string $date_created
 * @property string $company_stamp
 * @property int $status
 * @property int $approved_by_id
 */
class CompanyUnderstanding extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
	public $signaturefile;
	public $stampfile;
	
    public static function tableName()
    {
        return 'company_understanding';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['name', 'designation', 'signature', 'company_stamp'], 'string', 'max' => 200],
            [['date_created'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'name' => 'Name',
            'designation' => 'Designation',
            'signature' => 'Signature',
            'date_created' => 'Date Created',
            'company_stamp' => 'Company Stamp',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
	
	public function upload()
    {
		
        if ($this->validate()) {
			if($this->isNewRecord){
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				
				$imageNewName1 = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->signaturefile->baseName))) . $newRand.'.' . $this->signaturefile->extension;
				
				$imageNewName2 = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->stampfile->baseName))) . $newRand.$newRand.'.' . $this->stampfile->extension;
			
				$filePath1 = $rootPath .$imageNewName1 ;
				$filePath2 = $rootPath .$imageNewName2 ;
				$fileOriginPath1 = 'images/'.$imageNewName1;
				$fileOriginPath2 = 'images/'.$imageNewName2;
				
				$this->signature  = $fileOriginPath1;
				$this->company_stamp  = $fileOriginPath2;
				$this->save();
				$this->signaturefile->saveAs($filePath1);
				$this->stampfile->saveAs($filePath2);
			}
            return true;
        } else {
            return false;
        }
    }
	
}
