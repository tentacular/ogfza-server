<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_vendor_service".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $name
 * @property string $address
 * @property string $service
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalVendorService extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_vendor_service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'status'], 'integer'],
            [['name', 'address', 'service'], 'required'],
            [['date_created'], 'safe'],
            [['name', 'address', 'service', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'name' => 'Name',
            'address' => 'Address',
            'service' => 'Service',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
