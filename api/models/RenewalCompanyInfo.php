<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_company_info".
 *
 * @property int $id
 * @property int $renewal_id
 * @property string|null $company_name
 * @property string|null $fze_no
 * @property string|null $tin
 * @property string $date_created
 * @property int $status
 */
class RenewalCompanyInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_company_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id'], 'required'],
            [['renewal_id', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['company_name', 'fze_no', 'tin'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    { 
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'company_name' => 'Company Name',
            'fze_no' => 'Fze No',
            'tin' => 'Tin',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
