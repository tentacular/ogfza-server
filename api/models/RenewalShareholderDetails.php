<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "renewal_shareholder_details".
 *
 * @property int $id
 * @property int|null $renewal_id
 * @property string $name
 * @property string $nationality
 * @property int|null $number_of_shares
 * @property string $ocupation
 * @property string|null $resident
 * @property string $address
 * @property int|null $type
 * @property string|null $comment
 * @property string|null $date_created
 * @property int|null $status
 */
class RenewalShareholderDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'renewal_shareholder_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['renewal_id', 'number_of_shares', 'type', 'status'], 'integer'],
            [['name', 'nationality', 'ocupation', 'address'], 'required'],
            [['date_created'], 'safe'],
            [['name', 'nationality', 'ocupation', 'resident', 'address', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'renewal_id' => 'Renewal ID',
            'name' => 'Name',
            'nationality' => 'Nationality',
            'number_of_shares' => 'Number Of Shares',
            'ocupation' => 'Ocupation',
            'resident' => 'Resident',
            'address' => 'Address',
            'type' => 'Type',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'status' => 'Status',
        ];
    }
}
