<?php

namespace api\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "share_holders".
 *
 * @property int $id
 * @property int $root_id
 * @property string $name
 * @property string $address
 * @property string $number
 * @property string $signarure
 * @property int $status
 * @property int $approved_by_id
 */
class ShareHolders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
	
	public $imageFile;
	
    public static function tableName()
    {
        return 'share_holders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['root_id', 'status', 'approved_by_id'], 'integer'],
            [['name', 'address', 'number', 'signarure'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_id' => 'Root ID',
            'name' => 'Name',
            'address' => 'Address',
            'number' => 'Number',
            'signarure' => 'Signarure',
            'status' => 'Status',
            'approved_by_id' => 'Approved By ID',
        ];
    }
	
	public function upload()
    {
		
        if ($this->validate()) {
			if($this->isNewRecord){
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewName = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFile->baseName))) . $newRand.'.' . $this->imageFile->extension;
			
				$filePath = $rootPath .$imageNewName ;
				$fileOriginPath = 'images/'.$imageNewName;
				$this->signarure  = $fileOriginPath;
				$this->save();
				$this->imageFile->saveAs($filePath);
			}else{
				if(empty($this->imageFile)){
					$this->save();
				}else{
				$newRand = rand(1,1000000000001010);
				$rootPath = '@api/web/uploads/images/';
				$imageNewName = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ','_',trim($this->imageFile->baseName))) . $newRand.'.' . $this->imageFile->extension;
			
				$filePath = $rootPath .$imageNewName ;
				$fileOriginPath = 'images/'.$imageNewName;
				$this->signarure  = $fileOriginPath;
				$this->save();
				$this->imageFile->saveAs($filePath);
				}
			}
			
            return true;
        } else {
            return false;
        }
    }
}
