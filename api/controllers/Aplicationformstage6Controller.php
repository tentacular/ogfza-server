<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;

use api\models\RootApplicant;
use api\models\CompanyUnderstanding;



use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class Aplicationformstage6Controller extends RestController
{
    /**
     * @inheritdoc
     */
	
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   
        
        $companyUnderstandingModel =  CompanyUnderstanding::find()->andWhere(["root_id" => $id])->all();
	
		$mergeModels = [
			"companyunderstanding" => $companyUnderstandingModel,  
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }
    
	//$2y$13$PzfAmuuaSUkmWRjelFAs9.DP.4.dqeX0nbEEzhwoRzveFnKT37GhG
	
	public function actionCreate()
    {
		
		$rootModel = RootApplicant::find()->andWhere(['id' => $_POST['root_id']])->one();
		
		$model = new CompanyUnderstanding();
		
		
		$rootModel->application_stage = 9;
		$model->root_id = $_POST['root_id'];
		$model->signaturefile = UploadedFile::getInstanceByName('signature'); 
		$model->stampfile = UploadedFile::getInstanceByName('stamp');
		$model->name = $_POST['name'];
		$model->date_created = $_POST['date'];
		$model->designation = $_POST['designation'];
		
		
		
		if( $model->upload() and $rootModel->save(false)  ){
			Yii::$app->api->sendSuccessResponse($rootModel->attributes);
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
		}
			
			
		
	}

 


	
}



