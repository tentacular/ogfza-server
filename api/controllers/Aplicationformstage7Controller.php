<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;

use api\models\RootApplicant;
use api\models\DocumentUpload;



use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class Aplicationformstage7Controller extends RestController
{
    /**
     * @inheritdoc
     */
	
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create','getcountry'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   
        
        $documentUploadModel =  DocumentUpload::find()->andWhere(["root_id" => $id])->andWhere(['!=','type','DPR / Notarised Document'])->all();
	
		$mergeModels = [
			"documentupload" => $documentUploadModel,  
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }

	
	public function actionCreate()
    {
		//Yii::$app->api->sendSuccessResponse([$_FILES]);
		$rootModel = RootApplicant::find()->andWhere(['id' => $_POST['root_id']])->one();
		
		$cacModel = new DocumentUpload();
		$memorandumModel = new DocumentUpload();
		$brochureModel = new DocumentUpload();
		$businessplanModel = new DocumentUpload();
		$financialprofileModel = new DocumentUpload();
		$pasonelprofileModel = new DocumentUpload();
		$auditModel = new DocumentUpload();
		$affidavitModel = new DocumentUpload();
		
		$cacModel->root_id = $_POST['root_id'];
		$cacModel->type = "CAC";
		$cacModel->imageFile = UploadedFile::getInstanceByName('cac');
		
		$memorandumModel->root_id = $_POST['root_id'];
		$memorandumModel->type = "Memorandum";
		$memorandumModel->imageFile = UploadedFile::getInstanceByName('memorandum');
		
		$brochureModel->root_id = $_POST['root_id'];
		$brochureModel->type = "Brochure";
		$brochureModel->imageFile = UploadedFile::getInstanceByName('brochure');
		
		$businessplanModel->root_id = $_POST['root_id'];
		$businessplanModel->type = "Business Plan";
		$businessplanModel->imageFile = UploadedFile::getInstanceByName('businessplan');
		
		$financialprofileModel->root_id = $_POST['root_id'];
		$financialprofileModel->type = "Financial Profile";
		$financialprofileModel->imageFile = UploadedFile::getInstanceByName('financialprofile');
		
		$pasonelprofileModel->root_id = $_POST['root_id'];
		$pasonelprofileModel->type = "Pasonel Profile";
		$pasonelprofileModel->imageFile = UploadedFile::getInstanceByName('pasonelprofile');
		
		$auditModel->root_id = $_POST['root_id'];
		$auditModel->type = "Audit";
		$auditModel->imageFile = UploadedFile::getInstanceByName('audit');
		
		$affidavitModel->root_id = $_POST['root_id'];
		$affidavitModel->type = "Affidavit";
		$affidavitModel->imageFile = UploadedFile::getInstanceByName('affidavit');

		$rootModel->application_stage = 8;
		
		if( $cacModel->upload() and $memorandumModel->upload() and $brochureModel->upload() and $businessplanModel->upload() and $financialprofileModel->upload() and $pasonelprofileModel->upload() and $auditModel->upload() and $affidavitModel->upload() and $rootModel->save(false)  ){
			Yii::$app->api->sendSuccessResponse($rootModel->attributes);
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
		}
			
			
		
	}
	
	public function actionGetcountry(){
		$rootModel = RootApplicant::find()->andWhere(['id' => $this->request['root_id']])->one();
		
		if(!empty($rootModel) ){
			$data = $rootModel->attributes;
			$data['registrationcountry'] = $rootModel->companycountryofregistration->value;
			Yii::$app->api->sendSuccessResponse($data);
		}else{
			Yii::$app->api->sendFailedResponse(('Application Could not be processed'));
		}
	} 

 


	
}



