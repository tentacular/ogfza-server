<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use linslin\yii2\curl\Curl;

use api\models\CompanyCountryOfOrigin;
use api\models\CompanyAddress;
use api\models\CompanyFax;
use api\models\CompanyLocalAddress;
use api\models\CompanyLocalFax;
use api\models\CompanyLocalTel;
use api\models\CompanyLocalWebsite;
use api\models\CompanyName;
use api\models\CompanyTel;
use api\models\CompanyUnderstanding;
use api\models\CompanyWebsite;
use api\models\ContactLocalName;
use api\models\ContactName;
use api\models\CountryInOperation;
use api\models\DescriptionOfActivity;
use api\models\DocumentUpload;
use api\models\ExpectedStartDate;
use api\models\ExportMovement;
use api\models\ImportMovement;
use api\models\MouApprovalDate;
use api\models\MouNumberOfShares;
use api\models\MouRegLocation;
use api\models\natureOfBusinessOperation;
use api\models\ObjOfEstablishment;
use api\models\OgfzaBusinessActivities;
use api\models\PaymentInvoice;
use api\models\RegInNigeria;
use api\models\RequiredFacilities;
use api\models\ShareHolders;
use api\models\Shares;
use api\models\WitnessAddress;
use api\models\WitnessDoc;
use api\models\WitnessName;
use api\models\WitnessOccupation;
use api\models\WitnessSignature;
use api\models\Country;
use api\models\RenewalBusinessActivities;
use api\models\RenewalCompanycontactPerson;
use api\models\RenewalCompanyContactDetails;
use api\models\RenewalCompanyInfo;
use api\models\RenewalCompanyInfoIncoporation;
use api\models\RenewalDetailsOfRegisteredDebenture;
use api\models\RenewalDirectorParticulars;
use api\models\RenewalExport;
use api\models\RenewalForeignDirectInvestment;
use api\models\RenewalImport;
use api\models\RenewalIncoporationType;
use api\models\RenewalOaths;
use api\models\RenewalRequiredDocumentpUload;
use api\models\RenewalShareholderDetails;
use api\models\RenewalShareDetails;
use api\models\RenewalSpaceSize;
use api\models\RenewalVendorService;
use api\models\Renewal;
use api\models\RootApplicant;

	



/**
 * Site controller
 */
class StatusController extends RestController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['authorize', 'register', 'accesstoken','index','username-validator','email-validator','number-validator','ref-username-validator','login','requestpasswordreset','resetpassword','generatewallet','getusers','country'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['applicantdetails','applicantlist','approve','decline','approvesection','declinesection','dashboardcouter','dashboardroute','register'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['authorize',  'accesstoken','login','requestpasswordreset','resetpassword','generatewallet',],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'logout' => ['GET','POST'],
                    'authorize' => ['POST'],
                    'register' => ['POST'],
                    'accesstoken' => ['POST'],
                    'me' => ['GET'],
                    'update-phonenumber' => ['PUT'], 
                    'update-email' => ['PUT'], 
                    'country' => ['GET'], 
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    
     public function actionCheckstatusap1($id){
        $model = CompanyCountryOfOrigin::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }

     public function actionCheckstatusap2($id){
        
        $model = CompanyAddress::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }

     }

     public function actionCheckstatusap3($id){
        
        $model = CompanyFax::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }

     }

     public function actionCheckstatusap4($id){
        
        $model = CompanyLocalAddress::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap5($id){
        
        $model = CompanyLocalFax::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }
     }
     public function actionCheckstatusap6($id){
       
        $model =  CompanyLocalTel::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap7($id){
        
        $model =  CompanyLocalWebsite::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap8($id){
        
        $model =  CompanyName::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap9($id){
        
        $model =  CompanyTel::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap10($id){
        
        $model =  CompanyUnderstanding::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap11($id){
        $model =  CompanyWebsite::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap12($id){
        
        $model =  RenewalBusinessActivities::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap13($id){
        
        $model =  ContactLocalName::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap14($id){
        
        $model =  CountryInOperation::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
        
     }
     public function actionCheckstatusap15($id){
        
        $model =  DescriptionOfActivity::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap16($id){
        
        $model =  DocumentUpload::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap17($id){
        
        $model =  ExpectedStartDate::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap18($id){
        
        $model =  ExportMovement::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap19($id){
        
        $model =  ImportMovement::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
        
        
     }
     public function actionCheckstatusap20($id){
        
        $model =  MouApprovalDate::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap21($id){
        
        $model =  MouRegLocation::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
        
     }
     public function actionCheckstatusap22($id){
        
        $model =  MouNumberOfShares::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap23($id){
        
        $model =  natureOfBusinessOperation::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap24($id){
        
        $model =  ObjOfEstablishment::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap25($id){
        
        $model =  OgfzaBusinessActivities::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
        
     }
     public function actionCheckstatusap26($id){
        
        $model =  RegInNigeria::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap27($id){
        
        $model =  RequiredFacilities::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap28($id){
        
        $model =  ShareHolders::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap29($id){
        
        $model =  Shares::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap30($id){
        
        $model =  WitnessAddress::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }

     public function actionCheckstatusap31($id){
        
        $model =  WitnessName::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap32($id){
        
        $model =  WitnessOccupation::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap33($id){
        
        $model =  WitnessSignature::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap34($id){
        
        $model =  RenewalCompanycontactPerson::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap35($id){
        
        $model =  RenewalCompanyContactDetails::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap36($id){
        
        $model =  RenewalCompanyInfo::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap37($id){
        
        $model =  RenewalCompanyInfoIncoporation::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap38($id){
        
        $model =  RenewalDetailsOfRegisteredDebenture::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap39($id){
        
        $model =  RenewalDirectorParticulars::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap40($id){
        
        $model =  RenewalExport::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap41($id){
        $model =  RenewalForeignDirectInvestment::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap42($id){
        
        $model =  RenewalImport::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap43($id){
        
        $model =  RenewalIncoporationType::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap44($id){
        
        $model =  RenewalOaths::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap45($id){
        
        $model =  RenewalRequiredDocumentpUload::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap46($id){
        
        $model =  RenewalShareholderDetails::find()->andWhere(["root_id" => $id])->all();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap47($id){
        $model =  RenewalShareDetails::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap48($id){
        
        $model =  RenewalSpaceSize::find()->andWhere(["root_id" => $id])->one();
        if($model->status == 1){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap49($id){
        $model =  RenewalVendorService::find()->andWhere(["root_id" => $id])->one();
        $status = 0;
        foreach($model as $key => $value){
            if($value->status != 1){
                $status += 1 ;
            }
        }
        if($status == 0){
            Yii::$app->api->sendSuccessResponse($model);
        }else{
            Yii::$app->api->sendFailedResponse('validation failed');
        }
     }
     public function actionCheckstatusap50(){

     }
     public function actionCheckstatusap51(){

     }
     public function actionCheckstatusap52(){

     }
     public function actionCheckstatusap53(){

     }

     public function getDetails($id){
        $model = Renewal::find()->andWhere(['id' =>$id])->all();
		if (\Yii::$app->user->can('screen')) {
			$data['priv'] = 1;
		}
		
		if (\Yii::$app->user->can('spvp')) {
			$data['priv'] = 2;
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$data['priv'] = 3;
		}
		
		if (\Yii::$app->user->can('ceop')) {
			$data['priv'] = 4;
		}
		if (\Yii::$app->user->can('accountp')) {
					$data['priv'] = 5;
				}
		$data['applicant'] = [];
		foreach($model as $value){
			$newValue = $value->attributes;
			$companyname = [
                "businessactivity" => $value->businessactivity,
                "companyContactDetails" => $value->companyContactDetails,
                "companyContactPerson" => $value->companyContactPerson,
                "companyInfo" => $value->companyInfo,
                "companyInfoIncoporation" => $value->companyInfoIncoporation,
                "registeredDebenture" => $value->registeredDebenture,
                "directorParticulars" => $value->directorParticulars,
                "export" => $value->export,
                "foreignDirectInvestment" => $value->foreignDirectInvestment,
                "import" => $value->import,
                "incoporationType" => $value->incoporationType,
                "oaths" => $value->oaths,
                "documentUpload" => $value->documentUpload,
                "shareDetails" => $value->shareDetails,
                "shareHoldersDetails" => $value->shareHoldersDetails,
                "spaceSize" => $value->spaceSize,
                "vendorService" => $value->vendorService
            ];
			$newArray = array_merge($newValue,$companyname);
			array_push($data['applicant'], $newArray);
			
				
		}
		return $data;
    }


     public function actionApprovesection($id){
		if($this->request['section'] == "renewalbusinessactivities"){
			
			$model = RenewalBusinessActivities::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalcompanycontactperson"){
			
			$model = RenewalCompanycontactPerson::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalcompanycontactdetails"){
			
			$model = RenewalCompanyContactDetails::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalcompanyinfo"){
			
			$model = RenewalCompanyInfo::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalcompanyinfoincoporation"){
			
			$model = RenewalCompanyInfoIncoporation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewaldetailsofregistereddebenture"){
			
			$model = RenewalDetailsOfRegisteredDebenture::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewaldirectorparticulars"){
			
			$model = RenewalDirectorParticulars::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalexport"){
			
			$model = RenewalExport::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalforeigndirectinvestment"){
			
			$model = RenewalForeignDirectInvestment::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalimport"){
			
			$model = RenewalImport::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalincoporationtype"){
			
			$model = RenewalIncoporationType::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewaloaths"){
			
			$model = RenewalOaths::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
        if($this->request['section'] == "renewalrequireddocumentpUload"){
			
			$model = RenewalRequiredDocumentpUload::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
        if($this->request['section'] == "renewalshareholderdetails"){
			
			$model = RenewalShareholderDetails::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
        if($this->request['section'] == "renewalsharedetails"){
			
			$model = RenewalShareDetails::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		
        if($this->request['section'] == "renewalspacesize"){
			
			$model = RenewalSpaceSize::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}


        if($this->request['section'] == "renewalvendorservice"){
			
			$model = RenewalVendorService::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		
	
				
	}

     public function actionDeclinesection($id){
		if($this->request['section'] == "renewalbusinessactivities"){

			
			$model = RenewalBusinessActivities::find()->andWhere(['id' => $id])->one();
            $renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalcompanycontactperson"){
			
			$model = RenewalCompanycontactPerson::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalcompanycontactdetails"){
			
			$model = RenewalCompanyContactDetails::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalcompanyinfo"){
			
			$model = RenewalCompanyInfo::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalcompanyinfoincoporation"){
			
			$model = RenewalCompanyInfoIncoporation::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewaldetailsofregistereddebenture"){
			
			$model = RenewalDetailsOfRegisteredDebenture::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewaldirectorparticulars"){
			
			$model = RenewalDirectorParticulars::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalexport"){
			
			$model = RenewalExport::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalforeigndirectinvestment"){
			
			$model = RenewalForeignDirectInvestment::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalimport"){
			
			$model = RenewalImport::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewalincoporationtype"){
			
			$model = RenewalIncoporationType::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}

        if($this->request['section'] == "renewaloaths"){
			
			$model = RenewalOaths::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
        if($this->request['section'] == "renewalrequireddocumentpUload"){
			
			$model = RenewalRequiredDocumentpUload::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
        if($this->request['section'] == "renewalshareholderdetails"){
			
			$model = RenewalShareholderDetails::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
        if($this->request['section'] == "renewalsharedetails"){
			
			$model = RenewalShareDetails::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		
        if($this->request['section'] == "renewalspacesize"){
			
			$model = RenewalSpaceSize::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}


        if($this->request['section'] == "renewalvendorservice"){
			
			$model = RenewalVendorService::find()->andWhere(['id' => $id])->one();
			$renewalModel = Renewal::find()->andWhere(['id' =>$model->renewal_id])->one();
            $rootModel = RootApplicant::find()->andWhere(['id' =>$renewalModel->root_id])->one();
            $rootModel->status =2;
            $renewalModel->status =2;
			if($model->status == 0 and $rootModel->save(false) and $renewalModel->save(false) ){
				$model->status =2;
				if($model->save(false)){
					$data = $this->getDetails($model->renewal_id);
                    Yii::$app->api->sendSuccessResponse($data);

				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->renewal_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		
	
				
	}
	 
	
}



