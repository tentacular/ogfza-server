<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use api\models\RenewalRequiredDocumentUpload;
use api\models\RenewalDetailsOfRegisteredDebenture;
use api\models\RootApplicant;
use api\models\Renewal;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class AplicationrenewaldebentureController extends RestController
{
    /**
     * @inheritdoc
     */
	public $emailStageStatus = 1;
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'], 
                    'create' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {
        
        $renewalDetailsOfRegisteredDebentureModel = RenewalDetailsOfRegisteredDebenture::find()->andWhere(["renewal_id" => $id])->one();
        $incomeModel = RenewalRequiredDocumentUpload::find()->andWhere(["renewal_id" => $id, "type_name" => "Income"])->one();
        $liabilityModel = RenewalRequiredDocumentUpload::find()->andWhere(["renewal_id" => $id, "type_name" => "Liability"])->one();
        $externalModel = RenewalRequiredDocumentUpload::find()->andWhere(["renewal_id" => $id, "type_name" => "External"])->one();
        $otherincomeModel = RenewalRequiredDocumentUpload::find()->andWhere(["renewal_id" => $id, "type_name" => "Otherincome"])->one();
    
		$mergeModels = [
            "renewaldetailsofregistereddebenture" => $renewalDetailsOfRegisteredDebentureModel,
            "income" => $incomeModel,
            "liability" => $liabilityModel,
            "external" => $externalModel,
            "otherincome" => $otherincomeModel,
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);
    }

    public function actionCreate()
    {
        
        
        $renewalRootModel =  Renewal::find()->andWhere(['id' => $_POST['renewal_id']])->one();
        $renewalDetailsOfRegisteredDebenture = new RenewalDetailsOfRegisteredDebenture();
        $renewalDetailsOfRegisteredDebenture -> details = $_POST['debenture'];
        $renewalDetailsOfRegisteredDebenture->renewal_id = $_POST['renewal_id'];
        $renewalDetailsOfRegisteredDebenture -> date_created = date("Y-m-d h:i:s");
        $renewalDetailsOfRegisteredDebenture -> status = 0;
        $finishedDynamic = false;
        $renewalRootModel->application_stage = 3;
        $documents = explode(',',$_POST['document'][0]);
        foreach($documents as $key => $value){
            $vals = json_decode($value);
            $model = new RenewalRequiredDocumentUpload();
            $model->renewal_id = $_POST['renewal_id'];
            $model->status = 0; 
            $model->type_name =  $value;
            $model->date_created = date("Y-m-d h:i:s");
            $model->imageFile = UploadedFile::getInstanceByName('documentfile'.$key);
            $model->upload();
			$finishedDynamic = true;
        }

        if($finishedDynamic && $renewalDetailsOfRegisteredDebenture->save(false) && $renewalRootModel->save(false)){
            Yii::$app->api->sendSuccessResponse($renewalRootModel->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('Application Could not be processed');
        }
		
		
        
    }

 


	
}



