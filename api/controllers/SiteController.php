<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\AuthorizationCodes;
use common\models\AccessTokens;

use api\models\SignupForm;
use api\models\AuthAssignment;
use api\models\RootApplicant;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use linslin\yii2\curl\Curl;
use frontend\models\PromoCodes;
use frontend\models\WalletLog;

use api\models\CompanyCountryOfOrigin;
use api\models\CompanyAddress;
use api\models\CompanyFax;
use api\models\CompanyLocalAddress;
use api\models\CompanyLocalFax;
use api\models\CompanyLocalTel;
use api\models\CompanyLocalWebsite;
use api\models\CompanyName;
use api\models\CompanyTel;
use api\models\CompanyUnderstanding;
use api\models\CompanyWebsite;
use api\models\ContactLocalName;
use api\models\ContactName;
use api\models\CountryInOperation;
use api\models\DescriptionOfActivity;
use api\models\DocumentUpload;
use api\models\ExpectedStartDate;
use api\models\ExportMovement;
use api\models\ImportMovement;
use api\models\MouApprovalDate;
use api\models\MouNumberOfShares;
use api\models\MouRegLocation;
use api\models\natureOfBusinessOperation;
use api\models\ObjOfEstablishment;
use api\models\OgfzaBusinessActivities;
use api\models\PaymentInvoice;
use api\models\RegInNigeria;
use api\models\RequiredFacilities;
use api\models\ShareHolders;
use api\models\Shares;
use api\models\WitnessAddress;
use api\models\WitnessDoc;
use api\models\WitnessName;
use api\models\WitnessOccupation;
use api\models\WitnessSignature;
use api\models\Country;
use api\models\Renewal;

	



/**
 * Site controller
 */
class SiteController extends RestController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['authorize', 'register', 'accesstoken','index','username-validator','email-validator','number-validator','ref-username-validator','login','requestpasswordreset','resetpassword','generatewallet','getusers','country'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['applicantdetails','applicantlist','approve','decline','approvesection','declinesection','dashboardcouter','dashboardroute','register'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['authorize',  'accesstoken','login','requestpasswordreset','resetpassword','generatewallet',],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'logout' => ['GET','POST'],
                    'authorize' => ['POST'],
                    'register' => ['POST'],
                    'accesstoken' => ['POST'],
                    'me' => ['GET'],
                    'update-phonenumber' => ['PUT'], 
                    'update-email' => ['PUT'], 
                    'country' => ['GET'], 
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($route)
    {
		//note get relationship for renewal so the status could be used to display urgent geen for renewal
		// note request has to be for current year
		if($route == 'approvedbyceo'){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage',3])->all();
		}
		
		if($route == 'declinebyceo'){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage_decline',3])->all();
		}
		
		if($route == 'startapprovalceo'){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 2])->all();
		}
		
		
		if($route == "approvedbylegal"){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage',2])->all();
		}
		
		if($route == "declinebylegal"){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage_decline',2])->all();
		}
		
		if($route == "startapprovallegal"){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 1])->all();
		}

		
		if($route == "spvaccepted"){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage',1])->all();
		}
		
		if($route == "spvdeclined"){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage_decline',1])->all();
		}
		
		if($route == "startpsvapprove"){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage_countter' => 34,'approval_stage' => 0])->all();
		}
		
		if($route == "screenning"){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 0])->all();
		}
		
		
		if($route == 'totaldeclined'){
			$model= RootApplicant::find()->andWhere(['>','approval_stage_decline', 0])->all();
		}
		
		if($route == 'totalpaid'){
			$model = RootApplicant::find()->andWhere(['approval_stage' => 4])->all();
		}
		
		if($route == 'totalapproved'){
			$model = RootApplicant::find()->andWhere(['approval_stage' => 5])->all();
		}
		
		if($route == 'totalapplicant'){
			
			$model = RootApplicant::find()->andWhere(['application_stage' =>9])->all();
		}
		
	
		if (\Yii::$app->user->can('screen')) {
			$data['priv'] = 1;
		}
		
		if (\Yii::$app->user->can('spvp')) {
			$data['priv'] = 2;
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$data['priv'] = 3;
		}
		
		if (\Yii::$app->user->can('ceop')) {
			$data['priv'] = 4;
		}
		
		$data['applicant'] = [];
		foreach($model as $value){
			$newValue = $value->attributes;
			$companyname = ['companyname' => $value->companyname->value,
						   ];
			$newArray = array_merge($newValue,$companyname);
			array_push($data['applicant'], $newArray);
			
				
		}
		
		Yii::$app->api->sendSuccessResponse($data);
		
    }

	public function actionCountry(){
		$model = Country::find()->all();
		Yii::$app->api->sendSuccessResponse($model);
	}
	
	
	 public function actionDashboardroute($route)
    {

		// if from is renewal sample return

		// {
		// 	"status": 1,
		// 	"result": {
		// 		"priv": 2,
		// 		"applicant": [
		// 			{
		// 				"id": 1,
		// 				"root_id": 1,
		// 				"email": "fire@abc.com",
		// 				"application_stage": 1,
		// 				"comment": null,
		// 				"date_created": "2022-04-04 10:52:39",
		// 				"status": 1,
		// 				"companyname": "squeakyklin nigeria limited ofshoaws",
		// 				"from": "renewal",
		// 				"root_applicant": {
		// 					"id": 1,
		// 					"email": "kingsonly13c@gmail.com",
		// 					"application_stage": 9,
		// 					"approval_stage": 4,
		// 					"approval_stage_countter": 34,
		// 					"status": 0,
		// 					"approval_stage_decline": 1,
		// 					"approval_stage_countter_decline": 61,
		// 					"reason": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, ",
		// 					"processing_fee_status": 0
		// 				}
		// 			}
		// 		]
		// 	}
		// }

		// for regular application

		// {
		// 	"status": 1,
		// 	"result": {
		// 		"priv": 2,
		// 		"applicant": [
		// 			{
		// 				"id": 1,
		// 				"email": "kingsonly13c@gmail.com",
		// 				"application_stage": 9,
		// 				"approval_stage": 4,
		// 				"approval_stage_countter": 34,
		// 				"status": 0,
		// 				"approval_stage_decline": 1,
		// 				"approval_stage_countter_decline": 61,
		// 				"reason": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, ",
		// 				"processing_fee_status": 0,
		// 				"companyname": "squeakyklin nigeria limited ofshoaws",
		// 				"from": "root_application"
		// 			},	
					
		// 		]
		// 	}
		// }
		
		
		if($route == 'approvedbyceo'){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage',3])->andWhere(['approval_stage_decline'=> 0])->all();
		}
		
		if($route == 'declinebyceo'){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage_decline',3])->all();
		}
		
		if($route == 'startapprovalceo'){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 2])->all();
		}
		
		


		
		if($route == "approvedbylegal"){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage',2])->andWhere(['approval_stage_decline'=> 0])->all();
		}
		
		if($route == "declinebylegal"){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage_decline',2])->all();
		}
		
		if($route == "startapprovallegal"){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 1])->all();
		}

		
		if($route == "spvaccepted"){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage',1])->andWhere(['approval_stage_decline'=> 0])->all();
		}
		
		if($route == "spvdeclined"){
			$model = RootApplicant::find()->andWhere(['>=','approval_stage_decline',1])->all();
		}
		
		if($route == "startpsvapprove"){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage_countter' => 34,'approval_stage' => 0])->all();
		}
		
		if($route == "screenning"){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 0])->all();
		}
		
		
		if($route == 'totaldeclined'){
			$model= RootApplicant::find()->andWhere(['>','approval_stage_decline', 0])->all();
		}
		
		if($route == 'totalpaid'){
			$model = RootApplicant::find()->andWhere(['approval_stage' => 4])->all();
		}
		
		if($route == 'totalapproved'){
			$model = RootApplicant::find()->andWhere(['approval_stage' => 5])->andWhere(['approval_stage_decline'=> 0])->all();
		}
		
		if($route == 'totalapplicant'){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9])->all();
		}

		if($route == 'totalRenewal'){
			// have to fetch applicant relationship
			$model = Renewal::find()->andWhere(["application_stage" => 6])->all();
		}
		
	
		if (\Yii::$app->user->can('screen')) {
			$data['priv'] = 1;
		}
		
		if (\Yii::$app->user->can('spvp')) {
			$data['priv'] = 2;
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$data['priv'] = 3;
		}
		
		if (\Yii::$app->user->can('ceop')) {
			$data['priv'] = 4;
		}
		
		$data['applicant'] = [];
		foreach($model as $value){
			$newValue = $value->attributes;
			if($route == "totalRenewal"){
				$companyname = ['companyname' => isset($value->rootApplicant->companyname->value)?$value->rootApplicant->companyname->value:$value->companyInfo->company_name,
				"from"=>"renewal",
				"root_applicant" => $value->rootApplicant
						   ];
			}else{
				$companyname = ['companyname' => isset($value->companyname->value)?$value->companyname->value:"Company Name Not Available",
				"from"=>"root_application"
						   ];
			}
			
			$newArray = array_merge($newValue,$companyname);
			array_push($data['applicant'], $newArray);
			
				
		}
		
		Yii::$app->api->sendSuccessResponse($data);
		
    }
	
	public function actionDashboardcouter(){ 

		// the return we get for counter make sure to use
		// {
		// 	"status": 1,
		// 	"result": {
		// 		"spv": {
		// 			"Screening": 3,
		// 			"Await": 0,
		// 			"Decline": 4,
		// 			"Approve": 6,
		// 			"Renewal": 0
		// 		},
		// 		"legal": {
		// 			"Await": 0,
		// 			"Decline": 1,
		// 			"Approve": 4,
		// 			"Renewal": 0
		// 		},
		// 		"ceo": {
		// 			"Await": 0,
		// 			"Decline": 1,
		// 			"Approve": 4,
		// 			"Renewal": 0
		// 		},
		// 		"acc": {
		// 			"Await": 1,
		// 			"Decline": 0,
		// 			"Approve": 4,
		// 			"Renewal": 0
		// 		},
		// 		"gen": {
		// 			"all": 9,
		// 			"Decline": 4,
		// 			"Approve": 4,
		// 			"Paid": 4,
		// 			"Renewal": 0
		// 		}
		// 	}
		// }
		$allApplicant = RootApplicant::find()->andWhere(['application_stage' =>9])->all();
		$approvedApplicant = RootApplicant::find()->andWhere(['approval_stage' => 5,'approval_stage_decline'=> 0])->all();
		$paidApplicant = RootApplicant::find()->andWhere(['approval_stage' => 4])->all();
		$declined= RootApplicant::find()->andWhere(['>','approval_stage_decline', 0])->all();
		
		$screaning = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 0])->all();
		$awaitSpvApproval = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage_countter' => 34,'approval_stage' => 0])->all();
		$declinedBySpv = RootApplicant::find()->andWhere(['>=','approval_stage_decline',1])->all();
		$approvedBySpv = RootApplicant::find()->andWhere(['>=','approval_stage',1])->andWhere(['approval_stage_decline'=> 0])->all();
		
		
		$awaitLegalApproval = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 1])->all();
		$declinedByLegal = RootApplicant::find()->andWhere(['>=','approval_stage_decline',2])->all();
		$approvedByLegal = RootApplicant::find()->andWhere(['>=','approval_stage',2])->andWhere(['approval_stage_decline'=> 0])->all();
		
		$awaitCeoApproval = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 2])->all();
		$declinedByCeo = RootApplicant::find()->andWhere(['>=','approval_stage_decline',3])->all();
		$approvedByCeo = RootApplicant::find()->andWhere(['>=','approval_stage',3])->andWhere(['approval_stage_decline'=> 0])->all();
		
		$awaitAccApproval = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => 3])->all();
		$declinedByAcc = RootApplicant::find()->andWhere(['>=','approval_stage_decline',4])->all();
		$approvedByAcc = RootApplicant::find()->andWhere(['>=','approval_stage',4])->andWhere(['approval_stage_decline'=> 0])->all();
		$renewal = Renewal::find()->andWhere(["application_stage" => 6])->all();
		$data = [];
		$data['spv'] = [
			'Screening' => count($screaning),
			'Await' => count($awaitSpvApproval),
			'Decline' => count($declinedBySpv),
			'Approve' => count($approvedBySpv),
			'Renewal' => count($renewal),
		];
		
		$data['legal'] = [
			'Await' => count($awaitLegalApproval),
			'Decline' => count($declinedByLegal),
			'Approve' => count($approvedByLegal),
			'Renewal' => count($renewal),
		];
		
		$data['ceo'] = [
			'Await' => count($awaitCeoApproval),
			'Decline' => count($declinedByCeo),
			'Approve' => count($approvedByCeo),
			'Renewal' => count($renewal),
		];
		
		$data['acc'] = [
			'Await' => count($awaitAccApproval),
			'Decline' => count($declinedByAcc),
			'Approve' => count($approvedByAcc),
			'Renewal' => count($renewal),
		];
		
		$data['gen'] = [
			'all' => count($allApplicant),
			'Decline' => count($declined),
			'Approve' => count($approvedApplicant),
			'Paid' => count($approvedApplicant),
			'Renewal' => count($renewal),
		];
		
		if(!empty($data)){
			Yii::$app->api->sendSuccessResponse($data);
		}else{
			Yii::$app->api->sendFailedResponse('An isssue was encountered while fetching data');
		}
	}
	
	public function actionApplicantlist($id = null){
		if($id == null){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9])->all();
		}else{
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => $id])->all();
		}
		
		if (\Yii::$app->user->can('screen')) {
			$data['priv'] = 1;
		}
		
		if (\Yii::$app->user->can('spvp')) {
			$data['priv'] = 2;
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$data['priv'] = 3;
		}
		
		if (\Yii::$app->user->can('ceop')) {
			$data['priv'] = 4;
		}
		if (\Yii::$app->user->can('accountp')) {
					$data['priv'] = 5;
				}
		$data['applicant'] = [];
		foreach($model as $value){
			$newValue = $value->attributes;
			$companyname = ['companyname' => $value->companyname->value,
						   ];
			$newArray = array_merge($newValue,$companyname);
			array_push($data['applicant'], $newArray);
			
				
		}
		Yii::$app->api->sendSuccessResponse($data);
		

	}
	
	public function actionApplicantdetails($id = null){
		
		
		$model = RootApplicant::find()->andWhere(['id' =>$id])->all();
		if (\Yii::$app->user->can('screen')) {
			$data['priv'] = 1;
		}
		
		if (\Yii::$app->user->can('spvp')) {
			$data['priv'] = 2;
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$data['priv'] = 3;
		}
		
		if (\Yii::$app->user->can('ceop')) {
			$data['priv'] = 4;
		}
		if (\Yii::$app->user->can('accountp')) {
					$data['priv'] = 5;
				}
		$data['applicant'] = [];
		foreach($model as $value){
			$newValue = $value->attributes;
			$companyname = ['companyname' => $value->companyname,
							'companyaddress' => $value->companyaddress,
							'companycountryofregistration' => $value->companycountryofregistration,
							'companyfax' => $value->companyfax,
							'companylocaladdress' => $value->companylocaladdress,
							'companylocalfax' => $value->companylocalfax,
							'companylocaltel' => $value->companylocaltel,
							'companylocalwebsite' => $value->companylocalwebsite,
							'companytel' => $value->companytel,
							'companyunderstanding' => $value->companyunderstanding,
							'companywebsite' => $value->companywebsite,
							'contactlocalname' => $value->contactlocalname,
							'contactname' => $value->contactname,
							'countryinoperation' => $value->countryinoperation,
							'descriptionofactivity' => $value->descriptionofactivity,
							'documentupload' => $value->documentupload,
							'expectedstartdate' => $value->expectedstartdate,
							'exportmovement' => $value->exportmovement,
							'importmovement' => $value->importmovement,
							'mounumberofshares' => $value->mounumberofshares,
							'mouapprovaldate' => $value->mouapprovaldate,
							'moureglocation' => $value->moureglocation,
							'natureofbusinessoperation' => $value->natureofbusinessoperation,
							'objofestablishment' => $value->objofestablishment,
							'ogfzabusinessactivities' => $value->ogfzabusinessactivities,
							'paymentinvoice' => $value->paymentinvoice,
							'reginnigeria' => $value->reginnigeria,
							'requiredfacilities' => $value->requiredfacilities,
							'shareholders' => $value->shareholders,
							'shares' => $value->shares,
							'witnessaddress' => $value->witnessaddress,
							'witnessdoc' => $value->witnessdoc,
							'witnessname' => $value->witnessname,
							'witnessoccupation' => $value->witnessoccupation,
							'witnesssignature' => $value->witnesssignature,
						   ];
			$newArray = array_merge($newValue,$companyname);
			array_push($data['applicant'], $newArray);
			
				
		}
		Yii::$app->api->sendSuccessResponse($data);
		
	}
	
	public function actionApprove($id){
		$rootModel = RootApplicant::find()->andWhere(['id' =>$id])->one();
		if (\Yii::$app->user->can('spvp')) {
			$getLegal = AuthAssignment::find()->andWhere(['item_name' => "legal"])->one();
			if(!empty($getLegal)){
				Yii::$app
				->mailer
				->compose(
					['html' => 'updateusers'],
					['companyname' => $rootModel->companyname->value,'priv' => "Registry"]
				)
				->setFrom(["support@ogfza-registry.com" => " Admin"])
				->setTo($getLegal->user->email)
				->setSubject("Approval Update")
				->send();
			}
			$rootModel->approval_stage = 1 ;
			 
			
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$rootModel->approval_stage = 2;
			//send email to ceo
			$getLegal = AuthAssignment::find()->andWhere(['item_name' => "ceo"])->one();
			if(!empty($getLegal)){
				Yii::$app
				->mailer
				->compose(
					['html' => 'updateusers'],
					['companyname' => $rootModel->companyname->value,'priv' => "legal"]
				)
				->setFrom(["support@ogfza-registry.com" => " Admin"])
				->setTo($getLegal->user->email)
				->setSubject("Approval Update")
				->send();
			}
		}
		
		if (\Yii::$app->user->can('ceop')) {
			// check what stage and determine if it should be 3 or 5
			if($rootModel->approval_stage == 2){
				$getLegal = AuthAssignment::find()->andWhere(['item_name' => "account"])->one();
				if(!empty($getLegal)){
					Yii::$app
					->mailer
					->compose(
						['html' => 'updateusers'],
						['companyname' => $rootModel->companyname->value,'priv' => "ceo"]
					)
					->setFrom(["support@ogfza-registry.com" => " Admin"])
					->setTo($getLegal->user->email)
					->setSubject("Approval Update")
					->send();
				}
				$rootModel->approval_stage = 3 ;
			}else{
				Yii::$app
					->mailer
					->compose(
						['html' => 'updateapplicant'],
						['companyname' => $rootModel->companyname->value,'priv' => "ceo2",'url'=>'https://cert.ogfza-registry.com/index.php?r=site/cert&id='.$rootModel->id,"reason" =>"none"]
					)
					->setFrom(["support@ogfza-registry.com" => " Admin"])
					->setTo($rootModel->email)
					->setSubject("Approval Update")
					->send();
				$rootModel->approval_stage = 5 ;
			}
			
			//should be send email to applicant informing them the have passed the screaning
		}
		
		if (\Yii::$app->user->can('accountp')) {
			$getLegal = AuthAssignment::find()->andWhere(['item_name' => "ceo"])->one();
				if(!empty($getLegal)){
					Yii::$app
					->mailer
					->compose(
						['html' => 'updateusers'],
						['companyname' => $rootModel->companyname->value,'priv' => "account"]
					)
					->setFrom(["support@ogfza-registry.com" => " Admin"])
					->setTo($getLegal->user->email)
					->setSubject("Approval Update")
					->send();
				}
			$rootModel->approval_stage = 4;
			//should be send email to applicant informing them the have passed the screaning
		}
		if($rootModel->save(false)){
			$data['applicant_details'] = $this->getDetails($id);
			$data['applicant_list'] = $this->getApplicantList();
			Yii::$app->api->sendSuccessResponse($data);
		}else{
			Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
		}
	}
	
	public function actionDecline($id){
		$rootModel = RootApplicant::find()->andWhere(['id' =>$id])->one();
		$rootModel->reason = $this->request['reason'];
		
		if (\Yii::$app->user->can('spvp')) {
			$rootModel->approval_stage = 1 ;
			$rootModel->approval_stage_decline = 1 ;
			// send email to applicant with reason
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$rootModel->approval_stage = 2;
			$rootModel->approval_stage_decline = 2;
			// send email to applicant with reason
		}
		
		if (\Yii::$app->user->can('ceop')) {
			if($rootModel->approval_stage == 2){
				$rootModel->approval_stage = 3 ;
				$rootModel->approval_stage_decline = 3 ;
			}else{
				$rootModel->approval_stage = 5 ;
				$rootModel->approval_stage_decline = 5 ;
			}
			
			// send email to applicant with reason
		}
		if (\Yii::$app->user->can('accountp')) {
			$rootModel->approval_stage = 4 ;
			$rootModel->approval_stage_decline = 4 ;
			//should be send email to applicant informing them the have passed the screaning
		}
		if($rootModel->save(false)){
			$data['applicant_details'] = $this->getDetails($id);
			$data['applicant_list'] = $this->getApplicantList();
			Yii::$app
					->mailer
					->compose(
						['html' => 'updateapplicant'],
						['companyname' => $rootModel->companyname->value,'reason' => $this->request['reason'],'priv' => "none"]
					)
					->setFrom(["support@ogfza-registry.com" => " Admin"])
					->setTo($rootModel->email)
					->setSubject("Approval Update")
					->send();
			Yii::$app->api->sendSuccessResponse($data);
		}else{
			Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
		}
	}
	
	public function actionApprovesection($id){
		
		
		if($this->request['section'] == "companycountryofregistration"){
			
			$model = CompanyCountryOfOrigin::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "companyaddress"){
			
			$model = CompanyAddress::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
				
			}
			
			
		}
		
		if($this->request['section'] == "companyfax"){
			
			$model = CompanyFax::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "companylocaladdress"){
			
			$model = CompanyLocalAddress::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				
			}
			
			
		}
		
		if($this->request['section'] == "companylocalfax"){
			
			$model = CompanyLocalFax::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
			if($model->save(false)){
				$data = $this->getDetails($model->root_id);
				$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
				$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
				if($rootModel->save(false)){
					Yii::$app->api->sendSuccessResponse($data);
				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
				
			}else{
				Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
			}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "companylocaltel"){
			
			$model = CompanyLocalTel::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
				
			}
			
			
		}
		
		if($this->request['section'] == "companylocalwebsite"){
			
			$model = CompanyLocalWebsite::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
				
			}
			
			
		}
		
		if($this->request['section'] == "companyname"){
			
			$model = CompanyName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "companytel"){
			
			$model = CompanyTel::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "companyunderstanding"){
			
			$model = CompanyUnderstanding::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "companywebsite"){
			
			$model = CompanyWebsite::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "companylocalname"){
			
			$model = ContactLocalName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}	
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "contactname"){
			
			$model = ContactName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "contactlocalname"){
			
			$model = ContactLocalName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "countryinoperation"){
			
			$model = CountryInOperation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0 ){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "descriptionofactivity"){
			
			$model = DescriptionOfActivity::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}	
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "documentupload"){
			
			$model = DocumentUpload::find()->andWhere(['id' => $id])->one();
			
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = DocumentUpload::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
						if($rootModel->save(false)){
							Yii::$app->api->sendSuccessResponse($data);
						}else{
							Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
						}
					}else{
						Yii::$app->api->sendSuccessResponse($data);
					}



				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}	
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "expectedstartdate"){
			
			$model = ExpectedStartDate::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "exportmovement"){
			
			$model = ExportMovement::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = ExportMovement::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
						if($rootModel->save(false)){
							Yii::$app->api->sendSuccessResponse($data);
						}else{
							Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
						}
					}else{
						Yii::$app->api->sendSuccessResponse($data);
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "importmovement"){
			
			$model = ImportMovement::find()->andWhere(['id' => $id])->one();
			if($model->status == 0 ){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = ImportMovement::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
						if($rootModel->save(false)){
							Yii::$app->api->sendSuccessResponse($data);
						}else{
							Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
						}
					}else{
						Yii::$app->api->sendSuccessResponse($data);
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "mouapprovaldate"){
			
			$model = MouApprovalDate::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
			
		}
		
		if($this->request['section'] == "mounumberofshares"){
			
			$model = MouNumberOfShares::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "moureglocation"){
			
			$model = MouRegLocation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					$data = $this->getDetails($model->root_id);
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "natureofbusinessoperation"){
			
			$model = natureOfBusinessOperation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0 ){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "objofestablishment"){
			
			$model = ObjOfEstablishment::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "ogfzabusinessactivities"){
			
			$model = OgfzaBusinessActivities::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "reginnigeria"){
			
			$model = RegInNigeria::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "requiredfacilities"){
			
			$model = RequiredFacilities::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = RequiredFacilities::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
						if($rootModel->save(false)){
							Yii::$app->api->sendSuccessResponse($data);
						}else{
							Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
						}
					}else{
						Yii::$app->api->sendSuccessResponse($data);
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "shareholders"){
			
			$model = ShareHolders::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = ShareHolders::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
						if($rootModel->save(false)){
							Yii::$app->api->sendSuccessResponse($data);
						}else{
							Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
						}
					}else{
						Yii::$app->api->sendSuccessResponse($data);
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "shares"){
			
			$model = Shares::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnessaddress"){
			
			$model = WitnessAddress::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnessdoc"){
			
			$model = WitnessDoc::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnessname"){
			
			$model = WitnessName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnessoccupation"){
			
			$model = WitnessOccupation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
				
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnesssignature"){
			
			$model = WitnessSignature::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
				
	}
	
	public function actionDeclinesection($id){
		
		if($this->request['section'] == "companycountryofregistration"){
			
			$model = CompanyCountryOfOrigin::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companyaddress"){
			
			$model = CompanyAddress::find()->andWhere(['id' => $id])->one();
			if($model->status == 0 ){
				$model->status =1;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companyfax"){
			
			$model = CompanyFax::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companylocaladdress"){
			
			$model = CompanyLocalAddress::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companylocalfax"){
			
			$model = CompanyLocalFax::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companylocaltel"){
			
			$model = CompanyLocalTel::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companylocalwebsite"){
			
			$model = CompanyLocalWebsite::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companyname"){
			
			$model = CompanyName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companytel"){
			
			$model = CompanyTel::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companyunderstanding"){
			
			$model = CompanyUnderstanding::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companywebsite"){
			
			$model = CompanyWebsite::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "companylocalname"){
			
			$model = ContactLocalName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "contactname"){
			
			$model = ContactName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "contactlocalname"){
			
			$model = ContactLocalName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "countryinoperation"){
			
			$model = CountryInOperation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "descriptionofactivity"){
			
			$model = DescriptionOfActivity::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "documentupload"){
			
			$model = DocumentUpload::find()->andWhere(['id' => $id])->one();
			if($model->status == 0 ){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = DocumentUpload::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					}
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "expectedstartdate"){
			
			$model = ExpectedStartDate::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "exportmovement"){
			
			$model = ExportMovement::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = ExportMovement::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					}
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "importmovement"){
			
			$model = ImportMovement::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = ImportMovement::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					}
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "mouapprovaldate"){
			
			$model = MouApprovalDate::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "mounumberofshares"){
			
			$model = MouNumberOfShares::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "moureglocation"){
			
			$model = MouRegLocation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "natureofbusinessoperation"){
			
			$model = natureOfBusinessOperation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "objofestablishment"){
			
			$model = ObjOfEstablishment::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "ogfzabusinessactivities"){
			
			$model = OgfzaBusinessActivities::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "reginnigeria"){
			
			$model = RegInNigeria::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "requiredfacilities"){
			
			$model = RequiredFacilities::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = RequiredFacilities::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					}
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "shareholders"){
			
			$model = ShareHolders::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel = ShareHolders::find()->andWhere(['id' =>$model->root_id])->one();
					$newModel = RequiredFacilities::find()->andWhere(['root_id' => $model->root_id,'status' => 0])->all();
					if(empty($newModel)){
						$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					}
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "shares"){
			
			$model = Shares::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnessaddress"){
			
			$model = WitnessAddress::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnessdoc"){
			
			$model = WitnessDoc::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnessname"){
			
			$model = WitnessName::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnessoccupation"){
			
			$model = WitnessOccupation::find()->andWhere(['id' => $id])->one();
			if($model->status == 0){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		if($this->request['section'] == "witnesssignature"){
			
			$model = WitnessSignature::find()->andWhere(['id' => $id])->one();
			if($model->status == 0 ){
				$model->status =2;
				$model->approved_by_id = Yii::$app->user->identity['id'];
				if($model->save(false)){
					$data = $this->getDetails($model->root_id);
					$rootModel = RootApplicant::find()->andWhere(['id' =>$model->root_id])->one();
					$rootModel->approval_stage_countter = $rootModel->approval_stage_countter + 1;
					$rootModel->approval_stage_countter_decline = $rootModel->approval_stage_countter_decline + 1;
					if($rootModel->save(false)){
						Yii::$app->api->sendSuccessResponse($data);
					}else{
						Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
					}


				}else{
					Yii::$app->api->sendFailedResponse('An isssue was encountered while approving');
				}
			}else{
				$data = $this->getDetails($model->root_id);
				Yii::$app->api->sendSuccessResponse($data);
			}
			
		}
		
		// lets say 35
	}

  

    public function accessToken($authorizationCode)
    {
		
        if (!isset($authorizationCode)) {
            return null;
        }

        $authorization_code = $authorizationCode;

        $auth_code = AuthorizationCodes::isValid($authorization_code);
        if (!$auth_code) {
            return null;
        }

        $accesstoken = Yii::$app->api->createAccesstoken($authorization_code);
		return $accesstoken;



    }

    public function actionLogin()
    {
		// how to get an authorization code
		

		
        $model = new LoginForm();

        $model->attributes = $this->request;

		
        if ($model->validate() && $model->login()) {

            $auth_code = Yii::$app->api->createAuthorizationCode(Yii::$app->user->identity['id']);

            $user = User::find()->andWhere(['id'=> Yii::$app->user->identity['id']])->one();
			$data = $user->attributes;
			$getAccessToken = $this->accessToken($auth_code->code);
			if($getAccessToken != null){
				$data['access_token'] = $getAccessToken->token;
            	$data['expires_at'] = $getAccessToken->expires_at;
				if (\Yii::$app->user->can('screen')) {
					$data['priv'] = 1;
				}

				if (\Yii::$app->user->can('spvp')) {
					$data['priv'] = 2;
				}

				if (\Yii::$app->user->can('legalp')) {
					$data['priv'] = 3;
				}

				if (\Yii::$app->user->can('ceop')) {
					$data['priv'] = 4;
				}
				
				if (\Yii::$app->user->can('accountp')) {
					$data['priv'] = 5;
				}

           		Yii::$app->api->sendSuccessResponse($data,['message' => 'Login Successfull']);
			}
			Yii::$app->api->sendFailedResponse(['message' => 'Could not generate token']);
            
        } else {
			$message =  ' Sorry, something went wrong';
            Yii::$app->api->sendFailedResponse($message);
        }
    }
	
	 public function actionRequestpasswordreset()
    {
        $model = new PasswordResetRequestForm();
		$model->attributes = $this->request;
        if ($model->validate()) {
            if ($model->sendEmail()) {
				$user = User::find()->andWhere(['email' => $this->request['email']])->orWhere(['number' => $this->request['email']])->one();
				$data = $user->attributes;
				$data['otp'] = $user->password_reset_token;
				unset($data['auth_key']);	
				unset($data['password_hash']);
				unset($data['password_reset_token']);

				
				
				Notifications::sendSms($user->number,$data['otp']);
                Yii::$app->api->sendSuccessResponse($data,['message' => 'Token was sent']);
            } else {
                
				$message =  'Sorry, we are unable to reset password for the provided email address.';
            	Yii::$app->api->sendFailedResponse($message);
            }
			Yii::$app->api->sendFailedResponse('issue1');
        }
		Yii::$app->api->sendFailedResponse($model);

    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetpassword()
    {
        $model = new ResetPasswordForm($this->request['otp']);
		$model->attributes = $this->request;
       

        if ($model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            Yii::$app->api->sendSuccessResponse($model->attributes,['message' => 'New password saved']);
        }else{
			$message =  'Sorry, we are unable to reset password for the provided email address.';
            Yii::$app->api->sendFailedResponse($message);
		}

        
    }


    public function actionLogout()
    {
        $headers = Yii::$app->getRequest()->getHeaders();
        $access_token = $headers->get('x-access-token');

        
        $model = AccessTokens::findOne(['token' => $access_token]);

        if ($model->delete()) {

            Yii::$app->api->sendSuccessResponse(["Logged Out Successfully"]);

        } else {
            Yii::$app->api->sendFailedResponse("Invalid Request");
        }


    }
	
	public function getDetails($id){
		
		$model = RootApplicant::find()->andWhere(['id' =>$id])->all();
		
		if (\Yii::$app->user->can('screen')) {
			$data['priv'] = 1;
		}
		
		if (\Yii::$app->user->can('spvp')) {
			$data['priv'] = 2;
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$data['priv'] = 3;
		}
		
		if (\Yii::$app->user->can('ceop')) {
			$data['priv'] = 4;
		}
		if (\Yii::$app->user->can('accountp')) {
					$data['priv'] = 5;
				}
		
		$data['applicant'] = [];
		foreach($model as $value){
			$newValue = $value->attributes;
			$companyname = ['companyname' => $value->companyname,
							'companyaddress' => $value->companyaddress,
							'companycountryofregistration' => $value->companycountryofregistration,
							'companyfax' => $value->companyfax,
							'companylocaladdress' => $value->companylocaladdress,
							'companylocalfax' => $value->companylocalfax,
							'companylocaltel' => $value->companylocaltel,
							'companylocalwebsite' => $value->companylocalwebsite,
							'companytel' => $value->companytel,
							'companyunderstanding' => $value->companyunderstanding,
							'companywebsite' => $value->companywebsite,
							'contactlocalname' => $value->contactlocalname,
							'contactname' => $value->contactname,
							'countryinoperation' => $value->countryinoperation,
							'descriptionofactivity' => $value->descriptionofactivity,
							'documentupload' => $value->documentupload,
							'expectedstartdate' => $value->expectedstartdate,
							'exportmovement' => $value->exportmovement,
							'importmovement' => $value->importmovement,
							'mounumberofshares' => $value->mounumberofshares,
							'mouapprovaldate' => $value->mouapprovaldate,
							'moureglocation' => $value->moureglocation,
							'natureofbusinessoperation' => $value->natureofbusinessoperation,
							'objofestablishment' => $value->objofestablishment,
							'ogfzabusinessactivities' => $value->ogfzabusinessactivities,
							'paymentinvoice' => $value->paymentinvoice,
							'reginnigeria' => $value->reginnigeria,
							'requiredfacilities' => $value->requiredfacilities,
							'shareholders' => $value->shareholders,
							'shares' => $value->shares,
							'witnessaddress' => $value->witnessaddress,
							'witnessdoc' => $value->witnessdoc,
							'witnessname' => $value->witnessname,
							'witnessoccupation' => $value->witnessoccupation,
							'witnesssignature' => $value->witnesssignature,
						   ];
			$newArray = array_merge($newValue,$companyname);
			array_push($data['applicant'], $newArray);
			
				
		}
		
		return $data;
	}
	
	public function getApplicantList($id = null){
		if($id == null){
			$model = RootApplicant::find()->andWhere(['application_stage' =>9])->all();
		}else{
			$model = RootApplicant::find()->andWhere(['application_stage' =>9,'approval_stage' => $id])->all();
		}
		
		if (\Yii::$app->user->can('screen')) {
			$data['priv'] = 1;
		}
		
		if (\Yii::$app->user->can('spvp')) {
			$data['priv'] = 2;
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$data['priv'] = 3;
		}
		
		if (\Yii::$app->user->can('ceop')) {
			$data['priv'] = 4;
		}
		if (\Yii::$app->user->can('accountp')) {
					$data['priv'] = 5;
				}
		$data['applicant'] = [];
		foreach($model as $value){
			$newValue = $value->attributes;
			$companyname = ['companyname' => $value->companyname->value,
						   ];
			$newArray = array_merge($newValue,$companyname);
			array_push($data['applicant'], $newArray);
			
				
		}
		return $data;
	}
	
	

    function smsSend($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message){
        
        $url =
        "http://www.smslive247.com/http/index.aspx?" . "cmd=sendquickmsg"
        . "&owneremail=" . UrlEncode($owneremail)
        . "&subacct=" . UrlEncode($subacct)
        . "&subacctpwd=" . UrlEncode($subacctpwd) . "&message=" . UrlEncode($message)."&sendto=".UrlEncode($sendto)."&sender=".UrlEncode('kings2');
        /* call the URL */



        if ($f = @fopen($url, "r")){ 
            $answer = fgets($f, 255);
            if (substr($answer, 0, 1) == "+"){
                
            } elseif($answer){
                
            } else {
                
            }
        } else {
            
        }


        
        //send email
    }
	
	public function actionRegister()
    {
		

        $model = new SignupForm();
        
        $model->attributes = $this->request;
		
		
        if ( $user = $model->signup()) {
			
			
			Yii::$app->api->sendSuccessResponse($user->attributes);

            
        }else {
			$message =  'Username , email or phone number has already been used';
            Yii::$app->api->sendFailedResponse($message);
        }
		
		

    }
	
	public function actionGetusers(){
		$model = User::find()->all();
		Yii::$app->api->sendSuccessResponse($model);	
	}
	
	
	
}



