<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use frontend\models\Address;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;

use common\models\User;
/**
 * Site controller
 */
class UserController extends RestController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['updateprofile'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'logout' => ['GET'],
                    'authorize' => ['POST'],
                    'register' => ['POST'],
                    'accesstoken' => ['POST'],
                    'me' => ['GET'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $model = User::find()->andWhere(['id' => $id])->one();
		if($model){
			$messageArray = ['message' => 'Registered Successfully'];
			Yii::$app->api->sendSuccessResponse($model->attributes,$messageArray);
		}else{
			Yii::$app->api->sendFailedResponse('Sorry, something went wrong !');
		}
        //  return $this->render('index');
    }
	
	public function actionUpdateprofile($id){
		$model = User::find()->andWhere(['id' => $id])->one();
		
		$model->attributes = $this->request;
        if ($model->save(false)) {
			$newData = User::find()->andWhere(['id' => $id])->one();
			$data = $newData->attributes;
			$messageArray = ['message' => 'Registered Successfully'];
           	Yii::$app->api->sendSuccessResponse($data,$messageArray);
        } else {
           
			Yii::$app->api->sendFailedResponse($model->save(false));
        }
	}
   
	
	


    
	
}
