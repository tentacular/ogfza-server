<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\AuthorizationCodes;
use common\models\AccessTokens;

use api\models\SignupForm;
use api\models\RenewalCompanyContactDetails;
use api\models\RenewalCompanycontactPerson;
use api\models\RenewalCompanyInfo;
use api\models\RenewalCompanyInfoIncoporation;
use api\models\RootApplicant;
use api\models\Renewal;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;


/**
 * Site controller
 */
class AplicationrenewalcompanyinfoController extends RestController
{
    /**
     * @inheritdoc
     */
	public $emailStageStatus = 1;
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'], 
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   

        $renewalCompanyInfoModel = RenewalCompanyInfo::find()->andWhere(["renewal_id" => $id])->one();
        $renewalCompanyContactDetailsModel = RenewalCompanyContactDetails::find()->andWhere(["renewal_id" => $id])->all();
        $renewalCompanyInfoIncoporationModel = RenewalCompanyInfoIncoporation::find()->andWhere(["renewal_id" => $id])->all();
        $renewalCompanycontactPersonModel = RenewalCompanycontactPerson::find()->andWhere(["renewal_id" => $id])->all();
    
		$mergeModels = [
            "renewalcompanyinfo" => $renewalCompanyInfoModel,
            "renewalcompanycontactdetails" => $renewalCompanyContactDetailsModel,
            "renewalCompanyInfoincoporation" => $renewalCompanyInfoIncoporationModel,
            "renewalCompanycontactPerson" => $renewalCompanycontactPersonModel
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }
    public function actionCreate()
    {   
        
        $renewalRootModel = Renewal::find()->andWhere(['id' => $this->request['renewal_id']])->one();
        $renewalRootModel->application_stage = 2;
        $renewalCompanyInfo = new RenewalCompanyInfo();
        $renewalCompanyInfo->attributes = $this->request;
        $renewalCompanyInfo->date_created = date("Y-m-d h:i:s") ;
        $renewalCompanyInfo->renewal_id = $this->request['renewal_id'];
		$renewalCompanyInfo->status = 0;

        $renewalCompanyContactDetails = new RenewalCompanyContactDetails();
        $renewalCompanyContactDetails->attributes = $this->request;
        $renewalCompanyContactDetails->date_created = date("Y-m-d h:i:s") ;
        $renewalCompanyContactDetails->renewal_id = $this->request['renewal_id'];
		$renewalCompanyContactDetails->status = 0;


        $incorporationType = false;
        foreach($this->request['incoporationtype'] as $key => $value){

			$renewalCompanyInfoIncoporationModel = new RenewalCompanyInfoIncoporation();
			$renewalCompanyInfoIncoporationModel->renewal_id = $this->request['renewal_id']  ;
			$renewalCompanyInfoIncoporationModel->value = $value['value'];
			$renewalCompanyInfoIncoporationModel->date_created = date("Y-m-d h:i:s") ;
			$renewalCompanyInfoIncoporationModel->status = 0;
            $renewalCompanyInfoIncoporationModel->save(false);
            
            $incorporationType = $key."  is this  ".count($this->request['incoporationtype']);
            if($key == count($this->request['incoporationtype']) - 1){
                
                $incorporationType = true;
        }
    }   

        $renewalCompanycontactPersonValidator = false;
        foreach($this->request['contact_person'] as $key => $value){
            $renewalCompanycontactPerson = new RenewalCompanycontactPerson();
            $vals = $value;

            $renewalCompanycontactPerson->renewal_id = $this->request['renewal_id']  ;
            $renewalCompanycontactPerson->name = $vals['name'];
            $renewalCompanycontactPerson->email = $vals['email'];
            $renewalCompanycontactPerson->phone = $vals['phonenumber'];
            $renewalCompanycontactPerson->date_created = date("Y-m-d h:i:s") ;
            $renewalCompanycontactPerson->status = 0;
            $renewalCompanycontactPerson->save(false);
            if($key == count($this->request['contact_person']) - 1){
                $renewalCompanycontactPersonValidator = true;
            }
			
		}
        if($renewalCompanycontactPersonValidator 
        and $incorporationType 
        and $renewalCompanyInfo->save(false)
        and $renewalCompanyContactDetails->save(false)
        and $renewalRootModel->save(false)){
            Yii::$app->api->sendSuccessResponse($renewalRootModel->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('Application Could not be processed');
        }

		
	}
		
		
        
    

 


	
}



