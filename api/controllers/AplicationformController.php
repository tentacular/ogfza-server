<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\AuthorizationCodes;
use common\models\AccessTokens;

use api\models\SignupForm;
use api\models\RootApplicant;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;


/**
 * Site controller
 */
class AplicationformController extends RestController
{
    /**
     * @inheritdoc
     */
	public $emailStageStatus = 1;
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['authorize', 'register', 'accesstoken','login','requestpasswordreset','resetpassword','generatewallet'],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'authorize' => ['POST'],
                    'register' => ['POST'],
                    'accesstoken' => ['POST'],
                    'me' => ['GET'],
                    'update-phonenumber' => ['PUT'], 
                    'update-email' => ['PUT'], 
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex()
    {
		$model = RootApplicant::find()->andWhere(['email' => $this->request['email']])->one();
		if($model){
			Yii::$app->api->sendSuccessResponse($model->attributes);
		}else{
			$model = new RootApplicant();
			$model->email = $this->request['email'];
            $renewalRootModel->application_type = $this->request['application_type'];
			$model->status = 0;
			$model->application_stage = $this->emailStageStatus;
			$model->approval_stage = 0;
			$model->processing_fee_status = 0;

			if($model->save(false)){
				Yii::$app->api->sendSuccessResponse($model->attributes);
			}else{
			
				Yii::$app->api->sendFailedResponse('Application Could not be processed');
			}
		}
		
		
        
    }

    public function actionNewindex(){
        $model = RootApplicant::find()->andWhere(['email' => $this->request['email']])->one();
		if($model){
			Yii::$app->api->sendSuccessResponse($model->attributes);
		}else{
			$model = new RootApplicant();
			$model->email = $this->request['email'];
			$model->status = 0;
			$model->application_stage = $this->emailStageStatus;
			$model->approval_stage = 0;
			$model->processing_fee_status = 0;

			if($model->save(false)){
				Yii::$app->api->sendSuccessResponse($model->attributes);
			}else{
			
				Yii::$app->api->sendFailedResponse('Application Could not be processed');
			}
		}
        
    }

 


	
}



