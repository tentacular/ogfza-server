<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;

use api\models\RootApplicant;
use api\models\Renewal;
use api\models\ApplicationProcessinfFee;



use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;
use common\models\User;


/**
 * Site controller
 */
class AplicationprocessingfeeController extends RestController
{
    /**
     * @inheritdoc
     */
	
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex()
    {
		
    }
	
	
	public function actionCreate()
    {
		$rootModel = RootApplicant::find()->andWhere(['id' => $_POST['root_id']])->one();
        if($rootModel->processing_fee_status == 1){
            $rootModel->status = 2; // this should be status indication that transaction have been completed
            //send the applicant an emain to come collect their Certn, thats if it not vendor 
            // update renewal status
            $renewalModel = Renewal::find()->andWhere(['root_id' => $_POST['root_id'] ,"status" => 0])->one();
            $renewalModel->status = 2;
            $renewalModel->save(false);
            
        }
		$rootModel->processing_fee_status = 1;
        $rootModel->application_stage =  9;

        // get all users and send them an email informing them of an awaiting applicattion

        $getUserModel = User::find()->all();
        foreach($getUserModel as $key => $value){
            $this->senEmailToUser($value->email,"New Application verification Notification",$rootModel->companyname->value);
        }


        // this page should go and update invoice that payment has been made for proccesing
		
		if($rootModel->save(false)  ){
            $this->senEmail($rootModel->email,"Application Update",$rootModel->companyname->value);
			Yii::$app->api->sendSuccessResponse($rootModel->attributes);
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
		}
			
			
		
	}

    public function senEmail($email,$subject,$companyname){
        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'updateapplicant', 'text' => 'updateapplicant'],
                ['companyname' => $companyname]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => "OGFZA" . ' robot'])
            ->setTo($email)
            ->setSubject($subject)
            ->send();
    }

    public function senEmailToUser($email,$subject,$companyname){
        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'updateusers', 'text' => 'updateusers'],
                ['companyname' => $companyname]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => "OGFZA" . ' robot'])
            ->setTo($email)
            ->setSubject($subject)
            ->send();
    }

    public function actionGetmail(){
        $this->senEmail("kingsonly13c@gmail.com","Application Update","Tentacular investment limited");
    }

    public function actionapproveFinalPayment()
    {
		$rootModel = RootApplicant::find()->andWhere(['id' => $_POST['root_id']])->one();
        if($rootModel->processing_fee_status == 1){
            $rootModel->status = 2; // this should be status indication that transaction have been completed
            //send the applicant an emain to come collect their Certn, thats if it not vendor 
            // update renewal status
            $renewalModel = Renewal::find()->andWhere(['root_id' => $_POST['root_id'] ,"status" => 0])->one();
            $renewalModel->status = 2;
            $renewalModel->save(false);
            
        }
		$rootModel->processing_fee_status = 1;
        $rootModel->application_stage =  9;

        // get all users and send them an email informing them of an awaiting applicattion

        $getUserModel = User::find()->all();
        foreach($getUserModel as $key => $value){
            $this->senEmailToUser($value->email,"New Application verification Notification",$rootModel->companyname->value);
        }


        // this page should go and update invoice that payment has been made for proccesing
		
		if($rootModel->save(false)  ){
            $this->senEmail($rootModel->email,"Application Update",$rootModel->companyname->value);
			Yii::$app->api->sendSuccessResponse($rootModel->attributes);
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
		}
			
			
		
	}
	
}



