<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use api\models\RenewalDirectorParticulars;
use api\models\RenewalShareDetails;
use api\models\RenewalShareholderDetails;
use api\models\Renewal;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;



/**
 * Site controller
 */
class AplicationrenewaldirectorshareholderdetailsController extends RestController
{
    /**
     * @inheritdoc
     */
	public $emailStageStatus = 1;
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'], 
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {
        
        $renewalDirectorParticularsModel = RenewalDirectorParticulars::find()->andWhere(["renewal_id" => $id])->all();
        
		$mergeModels = [
            "renewaldirectorparticulars" => $renewalDirectorParticularsModel,
            
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);
    }


    public function actionCreate()
    {   //Yii::$app->api->sendFailedResponse($_FILES);
        $renewalRootModel = Renewal::find()->andWhere(['id' => $_POST['renewal_id'] ])->one();
        $renewalRootModel->application_stage = 4;
        $noEntity = false;
        $nonentityDecode = json_decode($_POST['nonentity']);
        if(!empty($_POST['nonentity'])){
            foreach($nonentityDecode as $key => $value){
                $vals = $value;
                $directorPerticulars = new RenewalDirectorParticulars();
                $directorPerticulars->renewal_id = $_POST['renewal_id'];
                $directorPerticulars->name = $vals->name;
                $directorPerticulars->dob = $vals->dob ;
                $directorPerticulars->ocupation = $vals->occupation;
                $directorPerticulars->address = $vals->address;
                $directorPerticulars->nationality = $vals->nationality;
                $directorPerticulars->type = 1;
                $directorPerticulars->date_created = date('Y-m-d h:i:s');
                $directorPerticulars->status = 0;
                $directorPerticulars->save(false);
                if($key == count($nonentityDecode) - 1){
                    $noEntity = true;
                }
            }

        }else{
            $noEntity = true;
        }


        $entity = false;
        $entityDecode = json_decode($_POST['entity']);
   
        if(!empty($entityDecode )){
            foreach($entityDecode  as $key => $value){
                $vals = $value;
                $directorPerticulars = new RenewalDirectorParticulars();
                $directorPerticulars->renewal_id = $_POST['renewal_id'];
                $directorPerticulars->name = $vals->name;
                $directorPerticulars->address = $vals->address;
                $directorPerticulars->type = 2;
                $directorPerticulars->date_added = $vals->date;
                $directorPerticulars->date_created = date('Y-m-d h:i:s');
                $directorPerticulars->status = 0;
                $directorPerticulars->imageFile = UploadedFile::getInstanceByName('entitydocument'.$key);
                
                $directorPerticulars->upload();
                if($key == count($entityDecode) - 1){
                    $entity = true;
                }
                
            }

        }else{
            $entity = true;
        }
        
    
        $renewalShareDetails = new RenewalShareDetails();
        $renewalShareDetailsDecode = json_decode($_POST['renewalShareDetails']);
        $renewalShareDetails->renewal_id = $_POST['renewal_id'];
        $renewalShareDetails->nameauthorised_shares_capital = $renewalShareDetailsDecode->nameauthorised_shares_capital;
        $renewalShareDetails->dividend_into = $renewalShareDetailsDecode->dividend_into;
        $renewalShareDetails->dividend_ratio = $renewalShareDetailsDecode->dividend_ratio;
        $renewalShareDetails->issued_share_capital = $renewalShareDetailsDecode->issued_share_capital;
        $renewalShareDetails->paid_up_capital = $renewalShareDetailsDecode->paid_up_capital;
        $renewalShareDetails->date_created = date('Y-m-d h:i:s');
        $renewalShareDetails->status = 0;

        $noneCooporate = false;
        $renewalShareholderDetailsDecode = json_decode($_POST['renewalShareholderDetails']);
        if(!empty($renewalShareholderDetailsDecode)){
            foreach($renewalShareholderDetailsDecode as $key => $value){
                $renewalShareholderDetails = new RenewalShareholderDetails();
                $vals = $value;
                $renewalShareholderDetails->renewal_id = $_POST['renewal_id'];
                $renewalShareholderDetails->nationality = $vals->nationality;
                $renewalShareholderDetails->name = $vals->name;
                $renewalShareholderDetails->number_of_shares = $vals->numberofshare;
                $renewalShareholderDetails->resident = $vals->resident;
                $renewalShareholderDetails->address = $vals->address;
                $renewalShareholderDetails->type = 1;
                $renewalShareholderDetails->date_created = date("Y-m-d h:i:s");
                $renewalShareholderDetails->status = 0;
                $renewalShareholderDetails->save(false);
                if($key == count($renewalShareholderDetailsDecode) - 1){
                    $noneCooporate = true;
                }
            }
        }else{
            $noneCooporate = true;
        }

        $cooporate = false;
        $renewalShareholderDetailsCooporateDecode = json_decode($_POST['renewalShareholderDetailsCooporate']);
        if(!empty($renewalShareholderDetailsCooporateDecode)){
            foreach($renewalShareholderDetailsCooporateDecode as $key => $value){
                $renewalShareholderDetails = new RenewalShareholderDetails();
                $vals = $value;
                $renewalShareholderDetails->renewal_id = $_POST['renewal_id'];
                $renewalShareholderDetails->nationality = $vals->country;
                $renewalShareholderDetails->name = $vals->name;
                $renewalShareholderDetails->number_of_shares = $vals->share;
                $renewalShareholderDetails->address = $vals->address;
                $renewalShareholderDetails->type = 2;
                $renewalShareholderDetails->date_created = date("Y-m-d h:i:s");
                $renewalShareholderDetails->status = 0;
                $renewalShareholderDetails->save(false);
                if($key == count($renewalShareholderDetailsCooporateDecode) - 1){
                    $cooporate = true;
                }
            }
        }else{
            $cooporate = true;
        }

        if($noEntity && $entity && $noneCooporate && $cooporate &&  $renewalRootModel->save(false) && $directorPerticulars->save(false)){
            Yii::$app->api->sendSuccessResponse($renewalRootModel->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('Application Could not be processed');
        }
		
		
        
    }

 


	
}



