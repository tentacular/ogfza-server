<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use api\models\RenewalRequiredDocumentUpload;
use api\models\RenewalForeignDirectInvestment;
use api\models\RenewalSpaceSize;
use api\models\RenewalOaths;
use api\models\Renewal;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class AplicationrenewalrequireddocsController extends RestController
{
    /**
     * @inheritdoc
     */
	
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'], 
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex()
    {
        Yii::$app->api->sendSuccessResponse(['status' => 1]);
    }

    public function actionCreate()
    {
        $renewalRootModel = Renewal::find()->andWhere(['id' => $_POST['renewal_id'] ])->one();
        $renewalRootModel->application_stage = 6;
        
        // audit 
        $auditAccount = new RenewalRequiredDocumentUpload();//RenewalForeignDirectInvestment();
        $auditAccount->renewal_id = $_POST['renewal_id'];
        $auditAccount->status = 0;
        $auditAccount->type_name = "Audit";
        $auditAccount->imageFile = UploadedFile::getInstanceByName('auditAccount');
        $auditAccount->date_created = date("Y-m-d h:i:s");
        $auditAccoun->upload();

        // whtRemitance
        $whtRemitance = new RenewalRequiredDocumentUpload();//RenewalForeignDirectInvestment();
        $whtRemitance->renewal_id = $_POST['renewal_id'];
        $whtRemitance->status = 0;
        $whtRemitance->type_name = "WHT Remitance";
        $whtRemitance->imageFile = UploadedFile::getInstanceByName('whtRemitance');
        $whtRemitance->date_created = date("Y-m-d h:i:s");
        $whtRemitance->upload();

        // taxReturns

        $taxReturns = new RenewalRequiredDocumentUpload();//RenewalForeignDirectInvestment();
        $taxReturns->renewal_id = $_POST['renewal_id'];
        $taxReturns->status = 0;
        $taxReturns->type_name = "Tax Returns";
        $taxReturns->imageFile = UploadedFile::getInstanceByName('taxReturns');
        $taxReturns->date_created = date("Y-m-d h:i:s");
        $taxReturns->upload();


        //  localStaffPaye

        $localStaffPaye = new RenewalRequiredDocumentUpload();
        $localStaffPaye->renewal_id = $_POST['renewal_id'];
        $localStaffPaye->status = 0;
        $localStaffPaye->type_name = "Local Staff Paye";
        $localStaffPaye->imageFile = UploadedFile::getInstanceByName('localStaffPaye');
        $localStaffPaye->date_created = date("Y-m-d h:i:s");
        $localStaffPaye->upload();

        //  expatriate

        $expatriate = new RenewalRequiredDocumentUpload();
        $expatriate->renewal_id = $_POST['renewal_id'];
        $expatriate->status = 0;
        $expatriate->type_name = "Expatriate";
        $expatriate->imageFile = UploadedFile::getInstanceByName('expatriate');
        $expatriate->date_created = date("Y-m-d h:i:s");
        $expatriate->upload();


        //  newEmploye

        $newEmploye = new RenewalRequiredDocumentUpload();
        $newEmploye->renewal_id = $_POST['renewal_id'];
        $newEmploye->status = 0;
        $newEmploye->type_name = "New Employe";
        $newEmploye->imageFile = UploadedFile::getInstanceByName('newEmploye');
        $newEmploye->date_created = date("Y-m-d h:i:s");
        $newEmploye->upload();


        //  staffProfile

        $staffProfile = new RenewalRequiredDocumentUpload();
        $staffProfile->renewal_id = $_POST['renewal_id'];
        $staffProfile->status = 0;
        $staffProfile->type_name = "Staff Profile";
        $staffProfile->imageFile = UploadedFile::getInstanceByName('staffProfile');
        $staffProfile->date_created = date("Y-m-d h:i:s");
        $staffProfile->upload();

        //  landRegistrationCharge

        $landRegistrationCharge = new RenewalRequiredDocumentUpload();
        $landRegistrationCharge->renewal_id = $_POST['renewal_id'];
        $landRegistrationCharge->status = 0;
        $landRegistrationCharge->type_name = "Land Registration Charge";
        $landRegistrationCharge->imageFile = UploadedFile::getInstanceByName('landRegistrationCharge');
        $landRegistrationCharge->date_created = date("Y-m-d h:i:s");
        $landRegistrationCharge->upload();

        //  skillsAcquired

        $skillsAcquired = new RenewalRequiredDocumentUpload();
        $skillsAcquired->renewal_id = $_POST['renewal_id'];
        $skillsAcquired->status = 0;
        $skillsAcquired->type_name = "Skills Acquired";
        $skillsAcquired->imageFile = UploadedFile::getInstanceByName('skillsAcquired');
        $skillsAcquired->date_created = date("Y-m-d h:i:s");
        $skillsAcquired->upload();
        
        //  fzVehicle

        $fzVehicle = new RenewalRequiredDocumentUpload();
        $fzVehicle->renewal_id = $_POST['renewal_id'];
        $fzVehicle->status = 0;
        $fzVehicle->type_name = "FZ Vehicle";
        $fzVehicle->imageFile = UploadedFile::getInstanceByName('fzVehicle');
        $fzVehicle->date_created = date("Y-m-d h:i:s");
        $fzVehicle->upload();

        //  ozfzaAdminCharge

        $ozfzaAdminCharge = new RenewalRequiredDocumentUpload();
        $ozfzaAdminCharge->renewal_id = $_POST['renewal_id'];
        $ozfzaAdminCharge->status = 0;
        $ozfzaAdminCharge->type_name = "OGFZA Admin Charge";
        $ozfzaAdminCharge->imageFile = UploadedFile::getInstanceByName('ozfzaAdminCharge');
        $ozfzaAdminCharge->date_created = date("Y-m-d h:i:s");
        $ozfzaAdminCharge->upload();


        //  freeZoneFee

        $freeZoneFee = new RenewalRequiredDocumentUpload();
        $freeZoneFee->renewal_id = $_POST['renewal_id'];
        $freeZoneFee->status = 0;
        $freeZoneFee->imageFile = UploadedFile::getInstanceByName('freeZoneFee');
        $freeZoneFee->date_created = date("Y-m-d h:i:s");
        $freeZoneFee->upload();


        //  leaseAgreement

        $leaseAgreement = new RenewalRequiredDocumentUpload();
        $leaseAgreement->renewal_id = $_POST['renewal_id'];
        $leaseAgreement->status = 0;
        $leaseAgreement->imageFile = UploadedFile::getInstanceByName('leaseAgreement');
        $leaseAgreement->date_created = date("Y-m-d h:i:s");
        $leaseAgreement->upload();

        //  contractAgreement

        $contractAgreement = new RenewalRequiredDocumentUpload();
        $contractAgreement->renewal_id = $_POST['renewal_id'];
        $contractAgreement->status = 0;
        $contractAgreement->imageFile = UploadedFile::getInstanceByName('contractAgreement');
        $contractAgreement->date_created = date("Y-m-d h:i:s");
        $contractAgreement->upload();


        //  contractAgreement

        $contractAgreement = new RenewalRequiredDocumentUpload();
        $contractAgreement->renewal_id = $_POST['renewal_id'];
        $contractAgreement->status = 0;
        $contractAgreement->imageFile = UploadedFile::getInstanceByName('contractAgreement');
        $contractAgreement->date_created = date("Y-m-d h:i:s");
        $contractAgreement->upload();

        //  hseComplieanceFee

        $hseComplieanceFee = new RenewalRequiredDocumentUpload();
        $hseComplieanceFee->renewal_id = $_POST['renewal_id'];
        $hseComplieanceFee->status = 0;
        $hseComplieanceFee->imageFile = UploadedFile::getInstanceByName('hseComplieanceFee');
        $hseComplieanceFee->date_created = date("Y-m-d h:i:s");
        $hseComplieanceFee->upload();
        
        //  envioromentalServiceProvider

        $envioromentalServiceProvider = new RenewalRequiredDocumentUpload();
        $envioromentalServiceProvider->renewal_id = $_POST['renewal_id'];
        $envioromentalServiceProvider->status = 0;
        $envioromentalServiceProvider->imageFile = UploadedFile::getInstanceByName('envioromentalServiceProvider');
        $envioromentalServiceProvider->date_created = date("Y-m-d h:i:s");
        $envioromentalServiceProvider->upload();
        
        // RenewalSpaceSize
        $renewalSpaceSize = new RenewalSpaceSize();
        $renewalSpaceSize->status = 0;
        $renewalSpaceSize->stacking_area = $_POST["landSpace"] ;
        $renewalSpaceSize->warehouse =  $_POST["wareHouse"];
        $renewalSpaceSize->office_space =  $_POST["office"] ;
        $renewalSpaceSize->renewal_id = $_POST['renewal_id'];
        $renewalSpaceSize->date_created = date("Y-m-d h:i:s");

        //RenewalForeignDirectInvestment
        $renewalForeignDirectInvestment = new RenewalForeignDirectInvestment();
        $renewalForeignDirectInvestment->renewal_id = $_POST['renewal_id'] ;
        $renewalForeignDirectInvestment->volume =$_POST["volumeOfCapital"] ;
        $renewalForeignDirectInvestment->value =  $_POST["valueOfCapital"] ;
        $renewalForeignDirectInvestment->others = $_POST["additionalCapital"];
        $renewalForeignDirectInvestment->imageFile = UploadedFile::getInstanceByName('certOfCapImortance');
        
        //RenewalOaths
        $renewalOaths = new RenewalOaths();
        $renewalOaths->status = 0;
        $renewalOaths->renewal_id = $_POST['renewal_id'];
        $renewalOaths->name = $_POST["name"];
        $renewalOaths->designation = $_POST["designation"];
        $renewalOaths->phone = $_POST["telephone"];
        $renewalOaths->date_created =  $_POST["dateCreated"];
        $renewalOaths->imageFileDirectorSig = UploadedFile::getInstanceByName('directorSig');
        $renewalOaths->imageFileSecretarySig = UploadedFile::getInstanceByName('secretarySig');
        

        //return
        if($renewalOaths->upload() && $renewalRequiredDocumentUploadChecker && $renewalForeignDirectInvestment->upload() &&  $renewalSpaceSize->save(false) &&  $renewalRootModel->save(false)){
            Yii::$app->api->sendSuccessResponse($renewalRootModel->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('Application Could not be processed');
        }
		
		
        
    }

 


	
}



