<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;

use api\models\RootApplicant;
use api\models\ExpectedStartDate;
use api\models\RequiredFacilities;
use api\models\OgfzaBusinessActivities;


use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class Aplicationformstage4Controller extends RestController
{
    /**
     * @inheritdoc
     */
	
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   
        
        $expectedStartDateModel =  ExpectedStartDate::find()->andWhere(["root_id" => $id])->one();
		$requiredFacilitiesModel =  RequiredFacilities::find()->andWhere(["root_id" => $id])->all();
		$ogfzaBusinessActivitiesModel =  OgfzaBusinessActivities::find()->andWhere(["root_id" => $id])->one();
        
		$mergeModels = [
			"expectedstartdate" => $expectedStartDateModel,
            "requiredfacilities" => $requiredFacilitiesModel,
           "ogfzabusinessactivities" => $ogfzaBusinessActivitiesModel,  
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }
	
	
	public function actionCreate()
    {
		
		$rootModel = RootApplicant::find()->andWhere(['id' => $this->request['root_id']])->one();
		$expectedStartDateModel= new ExpectedStartDate();
		$requiredFacilities = new RequiredFacilities();
		$ogfzaBusinessActivities = new OgfzaBusinessActivities();
		
		


		
		
		// load model attributes  with value from client
		$expectedStartDateModel->root_id = $this->request['root_id'];
		$expectedStartDateModel->start_date = $this->request['expectedstartdate'];
		
		$ogfzaBusinessActivities->root_id = $this->request['root_id'];
		$ogfzaBusinessActivities->description = $this->request['businessactivity'];
		
		
	
		$measurment = [];
		$rootModel->application_stage = 6;
		if(!empty($this->request['measurment'])){
			foreach($this->request['measurment'] as $key => $value){
				
				$requiredFacilities = new RequiredFacilities();
				$requiredFacilities->root_id = $this->request['root_id'];
				$requiredFacilities->type = $value['type'];
				$requiredFacilities->quantity = $value['value'];
				$requiredFacilities->quantity_measurement = $value['mearsurment'];
				
				array_push($measurment,$requiredFacilities);
				
			}
		}else{
				$requiredFacilities = new RequiredFacilities();
				$requiredFacilities->root_id = $this->request['root_id'];
				$requiredFacilities->type = "none";
				$requiredFacilities->quantity = "none";
				$requiredFacilities->quantity_measurement = "none";
				
				array_push($measurment,$requiredFacilities);
		}
		
		
		
		if($expectedStartDateModel->save(false) and $ogfzaBusinessActivities->save(false) and $rootModel->save(false)){
			
				if(!empty($measurment)){
					foreach($measurment as $value){
						$value->save(false);
					}
				}
			Yii::$app->api->sendSuccessResponse($rootModel->attributes);
			
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
		}
	}

 


	
}



