<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;

use api\models\RootApplicant;
use api\models\CountryInOperation;
use api\models\DescriptionOfActivity;


use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class Aplicationformstage3Controller extends RestController
{
    /**
     * @inheritdoc
     */
	public $emailStatus = 1;
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   
        
        $descriptionOfActivityModel =  DescriptionOfActivity::find()->andWhere(["root_id" => $id])->one();
		$natureOfBusinessOperationModel =  CountryInOperation::find()->andWhere(["root_id" => $id])->one();
        
		$mergeModels = [
			"descriptionofactivity" => $descriptionOfActivityModel,
			"natureofbusinessoperation" => $natureOfBusinessOperationModel,
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }
	
	
	public function actionCreate()
    {
		
		$rootModel = RootApplicant::find()->andWhere(['id' => $this->request['root_id']])->one();
		
		
		$descriptionOfActivityModel = new DescriptionOfActivity();
		// load model attributes  with value from client
		$descriptionOfActivityModel->root_id = $this->request['root_id'];
		$descriptionOfActivityModel->description =  $this->request['description'];
		$countries = [];
		$rootModel->application_stage = 5;
		if(!empty($this->request['countries'])){
			foreach($this->request['countries'] as $key => $value){
				$countryInOperationModel = new CountryInOperation();
				$countryInOperationModel->root_id = $this->request['root_id'];
				$countryInOperationModel->country_id = $value['country_name'];
				array_push($countries,$countryInOperationModel);
				
			}
		}else{
				$countryInOperationModel = new CountryInOperation();
				$countryInOperationModel->root_id = $this->request['root_id'];
				$countryInOperationModel->country_id = "Not in any other country";
				array_push($countries,$countryInOperationModel);
		}
		
		
		
		if($descriptionOfActivityModel->save(false) and $rootModel->save(false)){
			
				if(!empty($countries)){
					foreach($countries as $value){
						$value->save(false);
					}
				}
			Yii::$app->api->sendSuccessResponse($rootModel->attributes);
			
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
		}
	}

 


	
}



