<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\AuthorizationCodes;
use common\models\AccessTokens;

use api\models\SignupForm;
use api\models\RootApplicant;
use api\models\Renewal;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;


/**
 * Site controller
 */
class AplicationrenewalformController extends RestController
{
    /**
     * @inheritdoc
     */
	public $emailStageStatus = 1;
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ["view","index"],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET'], 
                    'view' => ['GET'], 
                    'create' => ['POST'], 
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {
        //SORT_DESC
        $model = Renewal::find()->andWhere(["root_id" => $id])->orderBy(['date_created' => SORT_ASC])->all();
        Yii::$app->api->sendSuccessResponse($model);
    }

    public function actionCreate()
    {
		$model = RootApplicant::find()->andWhere(['email' => $this->request['email']])->one();
        $dateCheck = date("Y-08-d h:i:s");
        $renewalRootModel =  Renewal::find()->andWhere(['email' => $this->request['email']])->andWhere(['<=', 'date_created', $dateCheck])->one();
		if($model){
            // create renewal root model and update the stage
            if($renewalRootModel){
                Yii::$app->api->sendSuccessResponse($renewalRootModel->attributes);
            }else{
                $renewalRootModel = new Renewal();
                $renewalRootModel->email = $this->request['email'];
                $renewalRootModel->application_type = $this->request['application_type'];
                $renewalRootModel->date_created = date("Y-m-d h:i:s");
                $renewalRootModel->application_stage = 1;
                $renewalRootModel->status = 0;
                $renewalRootModel->root_id = $model->id;
                if($renewalRootModel->save(false)){
                    Yii::$app->api->sendSuccessResponse($renewalRootModel->attributes);
                }else{
                    Yii::$app->api->sendFailedResponse('Application Could not be processed');
                }
            }

		}else{
            // create renewal root model and also application root model
			$model = new RootApplicant();
			$model->email = $this->request['email'];
			$model->status = 0;
			$model->application_stage = $this->emailStageStatus;
			$model->approval_stage = 0;
            // save renewal root for specific year
            $renewalRootModel = new Renewal();
            $renewalRootModel->email = $this->request['email'];
            $renewalRootModel->date_created = date("Y-m-d h:i:s");
            $renewalRootModel->application_stage = 1;
            $renewalRootModel->status = 0;
			if($model->save(false)){
                $renewalRootModel->root_id = $model->id;
                if($renewalRootModel->save(false)){
                    Yii::$app->api->sendSuccessResponse($renewalRootModel->attributes);
                }else{
                    Yii::$app->api->sendFailedResponse('Application Could not be processed');
                }
				
			}else{
                
			
				Yii::$app->api->sendFailedResponse('Application Could not be processed');
			}
		}
		
		
        
    }

    public function actionView($id){
        $model = Renewal::find()->andWhere(['id' =>$id])->all();
		if (\Yii::$app->user->can('screen')) {
			$data['priv'] = 1;
		}
		
		if (\Yii::$app->user->can('spvp')) {
			$data['priv'] = 2;
		}
		
		if (\Yii::$app->user->can('legalp')) {
			$data['priv'] = 3;
		}
		
		if (\Yii::$app->user->can('ceop')) {
			$data['priv'] = 4;
		}
		if (\Yii::$app->user->can('accountp')) {
					$data['priv'] = 5;
				}
		$data['applicant'] = [];
		foreach($model as $value){
			$newValue = $value->attributes;
			$companyname = [
                "businessactivity" => $value->businessactivity,
                "companyContactDetails" => $value->companyContactDetails,
                "companyContactPerson" => $value->companyContactPerson,
                "companyInfo" => $value->companyInfo,
                "companyInfoIncoporation" => $value->companyInfoIncoporation,
                "registeredDebenture" => $value->registeredDebenture,
                "directorParticulars" => $value->directorParticulars,
                "export" => $value->export,
                "foreignDirectInvestment" => $value->foreignDirectInvestment,
                "import" => $value->import,
                "incoporationType" => $value->incoporationType,
                "oaths" => $value->oaths,
                "documentUpload" => $value->documentUpload,
                "shareDetails" => $value->shareDetails,
                "shareHoldersDetails" => $value->shareHoldersDetails,
                "spaceSize" => $value->spaceSize,
                "vendorService" => $value->vendorService
            ];
			$newArray = array_merge($newValue,$companyname);
			array_push($data['applicant'], $newArray);
			
				
		}
		Yii::$app->api->sendSuccessResponse($data);
    }



    



    

 


	
}



