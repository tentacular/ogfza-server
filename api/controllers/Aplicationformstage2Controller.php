<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;

use api\models\RootApplicant;
use api\models\DocumentUpload;
use api\models\natureOfBusinessOperation;


use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class Aplicationformstage2Controller extends RestController
{
    /**
     * @inheritdoc
     */
	public $emailStatus = 1;
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   
        $documentModel =  DocumentUpload::find()->andWhere(["root_id" => $id,"type" => "DPR / Notarised Document"])->one();
		$natureOfBusinessOperationModel =  natureOfBusinessOperation::find()->andWhere(["root_id" => $id])->one();
        
		
		$mergeModels = [
			"document" => $documentModel,
			"natureofbusiness" => $natureOfBusinessOperationModel,
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }
	
	
	
	public function actionCreate()
    {
		//Yii::$app->api->sendSuccessResponse($_POST);
		$rootModel = RootApplicant::find()->andWhere(['id' => $_POST['root_id']])->one();
		
		$documentModel = new DocumentUpload();
		$natureOfBusinessOperationModel = new natureOfBusinessOperation();
		// load model attributes  with value from client
		$documentModel->root_id = $_POST['root_id'];
		$documentModel->type = "DPR / Notarised Document";
		$rootModel->application_stage = 4;
		
		$natureOfBusinessOperationModel->root_id = $_POST['root_id'];
		$natureOfBusinessOperationModel->description = $_POST['description'];

		$documentModel->imageFile = UploadedFile::getInstanceByName('document');

		
			if($documentModel->upload() and $natureOfBusinessOperationModel->save(false) and $rootModel->save(false)){
				Yii::$app->api->sendSuccessResponse($rootModel->attributes);
			}else{
				Yii::$app->api->sendFailedResponse('Application Could not be processed');
			}
		}

 


	
}



