<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;

use api\models\RootApplicant;
use api\models\ExportMovement;
use api\models\RequiredFacilities;
use api\models\ImportMovement;


use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class Aplicationformstage5Controller extends RestController
{
    /**
     * @inheritdoc
     */
	
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   
        
        $importMovementModel =  ImportMovement::find()->andWhere(["root_id" => $id])->all();
		$exportMovementModel =  ExportMovement::find()->andWhere(["root_id" => $id])->all();
		

        
		$mergeModels = [
			"import" => $importMovementModel,
            "export" => $exportMovementModel,  
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }
	
	
	public function actionCreate()
    {
		
		$rootModel = RootApplicant::find()->andWhere(['id' => $this->request['root_id']])->one();
		
		
		$rootId = $this->request['root_id'];
		
		
		// load model attributes  with value from client
		
		
	
		$import = [];
		$export = [];
		$rootModel->application_stage = 7;
		if(!empty($this->request['import'])){
			foreach($this->request['import'] as $key => $value){
				
				$importMovementModel = new ImportMovement();
				$importMovementModel->root_id = $this->request['root_id'];
				$importMovementModel->route_type = $value['type'];
				$importMovementModel->cargo_type = $value['cargo'];
				$importMovementModel->tons = $value['tons'];
				$importMovementModel->container = $value['container'];
				$importMovementModel->value = $value['value'];
				
				array_push($import,$importMovementModel);
				
			}
		}
		
		if(!empty($this->request['export'])){
			foreach($this->request['export'] as $key => $value){
				
				$exportMovementModel= new ExportMovement();
				$exportMovementModel->root_id = $this->request['root_id'];
				$exportMovementModel->route_type = $value['type'];
				$exportMovementModel->cargo_type = $value['cargo'];
				$exportMovementModel->tons = $value['tons'];
				$exportMovementModel->container = $value['container'];
				$exportMovementModel->value = $value['value'];
				
				array_push($export,$exportMovementModel);
				
			}
		}
		
		
		$importSave = false;
		$exportSave = false;
		
			
			if(!empty($import)){
				foreach($import as $value){
					$value->save(false);
				}
				$importSave = true;
			}
				
			if(!empty($export)){
				foreach($export as $value){
					$value->save(false);
				}
					$exportSave = true;
			}
				
			if($importSave and $exportSave and $rootModel->save(false)){
				Yii::$app->api->sendSuccessResponse($rootModel->attributes);
			}else{
				Yii::$app->api->sendFailedResponse('Application Could not be processed');
			}
				
			
			
		
			
		
	}

 


	
}



