<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\AuthorizationCodes;
use common\models\AccessTokens;


use api\models\RootApplicant;
use api\models\CompanyName;

use api\models\CompanyAddress;
use api\models\CompanyFax;
use api\models\CompanyTel;
use api\models\CompanyWebsite;
use api\models\ContactName;

use api\models\CompanyLocalAddress;
use api\models\CompanyLocalFax;
use api\models\CompanyLocalTel;
use api\models\CompanyLocalWebsite;
use api\models\ContactLocalName;

use api\models\CompanyCountryOfOrigin;
use api\models\RegInNigeria;

use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;


/**
 * Site controller
 */
class Aplicationformstage1Controller extends RestController
{
    /**
     * @inheritdoc
     */
	public $emailStatus = 1;
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex()
    {
		$model = RootApplicant::find()->andWhere(['email' => $this->request['email']])->one();
		if($model){
			Yii::$app->api->sendSuccessResponse($model->attributes);
		}else{
			$model = new RootApplicant();
			$model->email = $this->request['email'];
			$model->status = 0;
			$model->application_stage = $this->emailStatus;
			$model->approval_stage = 0;
			if($model->save(false)){
				Yii::$app->api->sendSuccessResponse($model->attributes);
			}else{
			
				Yii::$app->api->sendFailedResponse('Application Could not be processed');
			}
		}
		
		
        
    }

	public function actionGetdetails($id)
    {   
		$rootModel = RootApplicant::find()->andWhere(['id' => $id])->one();
		$companyNameModel =  CompanyName::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyIntPhoneModel =  CompanyTel::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyIntFaxModel =  CompanyFax::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyIntWebsiteModel =  CompanyWebsite::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyIntAddressModel =  CompanyAddress::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyIntContactNameModel =  ContactName::find()->andWhere(["root_id" => $rootModel->id])->one();
		
		// local company detail
		$companyLocPhoneModel =  CompanyLocalTel::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyLocFaxModel =  CompanyLocalFax::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyLocWebsiteModel =  CompanyLocalWebsite::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyLocAddressModel =  CompanyLocalAddress::find()->andWhere(["root_id" => $rootModel->id])->one();
		$companyLocContactNameModel =  ContactLocalName::find()->andWhere(["root_id" => $rootModel->id])->one();
		
		// company registration location
		$companyRegistrationLocation =  CompanyCountryOfOrigin::find()->andWhere(["root_id" => $rootModel->id])->one();
		$registerInNigeria = RegInNigeria::find()->andWhere(["root_id" => $rootModel->id])->one();

		
		$mergeModels = [
			"copanyname" => $companyNameModel,
			"companyintphone" => $companyIntPhoneModel,
			"companyint" => $companyIntFaxModel,
			"companyintwebsite" => $companyIntWebsiteModel,
			"companyintaddress" => $companyIntAddressModel,
			"companyintcontactperson" => $companyIntContactNameModel,
			"companylocphone" => $companyLocPhoneModel,
			"companylocfax" => $companyLocFaxModel,
			"companylocwebsite" => $companyLocWebsiteModel,
			"companylocaddress" => $companyLocAddressModel,
			"companyloccontactperson" => $companyLocContactNameModel,
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }
	
	
	public function actionCreate()
    {
		//international contact details
			
		$companyNameModel = new CompanyName();
		$companyIntPhoneModel = new CompanyTel();
		$companyIntFaxModel = new CompanyFax();
		$companyIntWebsiteModel = new CompanyWebsite();
		$companyIntAddressModel = new CompanyAddress();
		$companyIntContactNameModel = new ContactName();
		
		// local company detail
		$companyLocPhoneModel = new CompanyLocalTel();
		$companyLocFaxModel = new CompanyLocalFax();
		$companyLocWebsiteModel = new CompanyLocalWebsite();
		$companyLocAddressModel = new CompanyLocalAddress();
		$companyLocContactNameModel = new ContactLocalName();
		
		// company registration location
		$companyRegistrationLocation = new CompanyCountryOfOrigin();
		$registerInNigeria = new RegInNigeria();
		$root = RootApplicant::find()->andWhere(['id' => $this->request['root_id']])->one();
		
		// load model attributes
		
		$companyNameModel->root_id = $this->request['root_id'];
		$companyNameModel->value = $this->request['company_name'];
			
		$companyIntPhoneModel->root_id = $this->request['root_id'];
		$companyIntPhoneModel->value = $this->request['phone_no'];
		
		$companyIntFaxModel->root_id = $this->request['root_id'];
		$companyIntFaxModel->value = $this->request['fax'];
		
		$companyIntWebsiteModel->root_id = $this->request['root_id'];
		$companyIntWebsiteModel->value = $this->request['website'];
			
		$companyIntAddressModel->root_id = $this->request['root_id'];
		$companyIntAddressModel->value = $this->request['address'];
			
		$companyIntContactNameModel->root_id = $this->request['root_id'];
		$companyIntContactNameModel->value = $this->request['contact_name'];
			
		$companyLocPhoneModel->root_id = $this->request['root_id'];
		$companyLocPhoneModel->value = $this->request['phone_no_local'];
			
		$companyLocFaxModel->root_id = $this->request['root_id'];
		$companyLocFaxModel->value = $this->request['fax_local'];
			
		$companyLocWebsiteModel->root_id = $this->request['root_id'];
		$companyLocWebsiteModel->value = $this->request['website_local'];
			
		$companyLocAddressModel->root_id = $this->request['root_id'];
		$companyLocAddressModel->value = $this->request['address_local'];
			
		$companyLocContactNameModel->root_id = $this->request['root_id'];
		$companyLocContactNameModel->value = $this->request['contact_name_local'];
		
		$companyRegistrationLocation->root_id = $this->request['root_id'];
		$companyRegistrationLocation->value = $this->request['registration_location'];
		$root->application_stage = 2;
		// save now
		
		

		if($companyNameModel->save(false) and $companyIntPhoneModel->save(false) and $companyIntFaxModel->save(false) and $companyIntWebsiteModel->save(false) and $companyIntAddressModel->save(false) and $companyIntContactNameModel->save(false) and $companyLocPhoneModel->save(false) and $companyLocFaxModel->save(false) and $companyLocWebsiteModel->save(false) and $companyLocAddressModel->save(false) and $companyLocContactNameModel->save(false) and $companyRegistrationLocation->save(false) and $registerInNigeria->save(false) and $root->save(false)){
			
			if($companyRegistrationLocation->value == 2){
				$registerInNigeria->root_id = $this->request['root_id'];
				$registerInNigeria->value = $this->request['register_in_nigeria'];
				$registerInNigeria->save(false);
			}else{
				$registerInNigeria->root_id = $this->request['root_id'];
				$registerInNigeria->value = 3;
				$registerInNigeria->save(false);
			}
			
			Yii::$app->api->sendSuccessResponse($root->attributes);
			
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
			
		}
		
        
    }

 


	
}



