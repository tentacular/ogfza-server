<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use api\models\RenewalBusinessActivities;
use api\models\RenewalVendorService;
use api\models\RenewalExport;
use api\models\RenewalImport;
use api\models\Renewal;
use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class AplicationrenewalbusinessactivityController extends RestController
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'], 
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   
        $businessActivitiesModel =  RenewalBusinessActivities::find()->andWhere(["renewal_id" => $id])->one();
        $vendorServiceModel =  RenewalVendorService::find()->andWhere(["renewal_id" => $id])->all();
        $exportModel =  RenewalExport::find()->andWhere(["renewal_id" => $id])->all();
        $importModel =  RenewalImport::find()->andWhere(["renewal_id" => $id])->all();
	
		$mergeModels = [
            "businessactivities" => $businessActivitiesModel,
            "vendorservice" => $vendorServiceModel,
            "export" => $exportModel,
            "import" => $importModel,
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }

    public function actionCreate()
    {
        $renewalRootModel = Renewal::find()->andWhere(['id' => $this->request['renewal_id'] ])->one();
        $renewalRootModel->application_stage = 5;

        $renewalBusinessActivitiesChecker = false;
        if(!empty($this->request['renewalBusinessActivities'])){
            foreach($this->request['renewalBusinessActivities'] as $key => $value){
                $renewalBusinessActivities = new RenewalBusinessActivities();
                $renewalBusinessActivities->renewal_id = $this->request['renewal_id'];
                $renewalBusinessActivities->activity = $value['activity'];
                $renewalBusinessActivities->approval_status = $value['approvedvendor'];
                $renewalBusinessActivities->status = 0;
                $renewalBusinessActivities->date_created = date("Y-m-d h:i:s");
                $renewalBusinessActivities->save(false);
                if($key == count($this->request['renewalBusinessActivities'])-1){
                    $renewalBusinessActivitiesChecker = true;
                }else{
                    $renewalBusinessActivitiesChecker = true;
                }

            }
        }

        $renewalVendorServiceChecker = false;
        if(!empty($this->request['renewalVendorService'])){
            foreach($this->request['renewalVendorService'] as $key => $value){
                $renewalVendorService = new RenewalVendorService();
                $renewalVendorService->renewal_id = $this->request['renewal_id'];
                $renewalVendorService->address = $value["addressvendor"];
                $renewalVendorService->name = $value["namevendor"];
                $renewalVendorService->service = $value["servicevendor"];
                $renewalVendorService->status = 0;
                $renewalVendorService->date_created = date("Y-m-d h:i:s");
                $renewalVendorService->save(false);
                if($key == count($this->request['renewalVendorService'])-1){
                    $renewalVendorServiceChecker = true;
                }else{
                    $renewalVendorServiceChecker = true;
                }

            }
        }

        $renewalExportChecker = false;
        if(!empty($this->request['renewalExport'])){
            foreach($this->request['renewalExport'] as $key => $value){
                $renewalExport = new RenewalExport();
                
                $renewalExport->renewal_id = $this->request['renewal_id'];
                $renewalExport->nature_of_export = $value["exported"];
                $renewalExport->value = $value["valexport"];
                $renewalExport->volume = $value["volexport"];

                $renewalExport->status = 0;
                $renewalExport->date_created = date("Y-m-d h:i:s");
                $renewalExport->save(false);
                if($key == count($this->request['renewalExport'])-1){
                    $renewalExportChecker = true;
                }else{
                    $renewalExportChecker = true;
                }

            }
        }

        $renewalImportChecker = false;
        if(!empty($this->request['renewalImport'])){
            foreach($this->request['renewalImport'] as $key => $value){
                $renewalImport = new RenewalImport();
                $renewalImport->renewal_id = $this->request['renewal_id'];
                $renewalImport->nature_of_import = $value["imported"];
                $renewalImport->value_oversea = $value["valoversea"];
                $renewalImport->value = $value["valueimport"];
                $renewalImport->value_ng = $value["valueimported"];
                $renewalImport->volum_oversea = $value["voloversea"];
                $renewalImport->volume = $value["volumeimport"];
                $renewalImport->volum_ng = $value["volumeimported"];
                $renewalImport->status = 0;
                $renewalImport->date_created = date("Y-m-d h:i:s");
                $renewalImport->save(false);
                if($key == count($this->request['renewalImport'])-1){
                    $renewalImportChecker = true;
                }else{
                    $renewalImportChecker = true;
                }

            }
        }


        if($renewalBusinessActivitiesChecker && $renewalVendorServiceChecker && $renewalExportChecker && $renewalImportChecker &&  $renewalRootModel->save(false)){
            Yii::$app->api->sendSuccessResponse($renewalRootModel->attributes);
        }else{
            Yii::$app->api->sendFailedResponse('Application Could not be processed');
        }
		
		
        
    }

}



