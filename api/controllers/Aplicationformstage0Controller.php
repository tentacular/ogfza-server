<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;

use api\models\RootApplicant;
use api\models\MouApprovalDate;
use api\models\MouNumberOfShares;
use api\models\MouRegLocation;
use api\models\ObjOfEstablishment;
use api\models\ShareHolders;
use api\models\Shares;
use api\models\WitnessAddress;
use api\models\WitnessDoc;
use api\models\WitnessName;
use api\models\WitnessOccupation;
use api\models\WitnessSignature;


use api\behaviours\Verbcheck;
use api\behaviours\Apiauth;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class Aplicationformstage0Controller extends RestController
{
    /**
     * @inheritdoc
     */
	
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['index','create','getcompanyname'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'], 
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'me','update-phonenumber','update-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'index' => ['GET','POST'],
                    'Create' => ['POST'],
                    
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {   
		
		$rootModel = RootApplicant::find()->andWhere(['id' => $id])->one();
		
		$mouOfficeModel =  MouRegLocation::find()->andWhere(["root_id" => $rootModel->id])->one();
		$mouObjectsOfEstablishedModel =  ObjOfEstablishment::find()->andWhere(["root_id" => $rootModel->id])->one();
		$authorizedShareModel =  Shares::find()->andWhere(["root_id" => $rootModel->id])->one();
		$totalSharesModel =  MouNumberOfShares::find()->andWhere(["root_id" => $rootModel->id])->one();
		$mouDateModel =  MouApprovalDate::find()->andWhere(["root_id" => $rootModel->id])->one();
		$witnessAddressModel =  WitnessAddress::find()->andWhere(["root_id" => $rootModel->id])->one();
		$witnessNameModel =  WitnessName::find()->andWhere(["root_id" => $rootModel->id])->one();
		$witnessOccupationModel = WitnessOccupation::find()->andWhere(["root_id" => $rootModel->id])->one();
		$witnessSignatureModel = WitnessSignature::find()->andWhere(["root_id" => $rootModel->id])->one();
		$shareHoldersModel = ShareHolders::find()->andWhere(["root_id" => $rootModel->id])->all();
		$mergeModels = [
			"mouoffice" => $mouOfficeModel,
			"objofstablished" => $mouObjectsOfEstablishedModel,
			"authorizedshare" => $authorizedShareModel,
			"totalshares" => $totalSharesModel,
			"moudate" => $mouDateModel,
			"witnessaddress" => $witnessAddressModel,
			"witnessname" => $witnessNameModel,
			"witnessoccupation" => $witnessOccupationModel,
			"witnesssignature" => $witnessSignatureModel,
			"shareholders" => $shareHoldersModel,
		];
		Yii::$app->api->sendSuccessResponse($mergeModels);

    }
	//$2y$13$PzfAmuuaSUkmWRjelFAs9.DP.4.dqeX0nbEEzhwoRzveFnKT37GhG
	
	public function actionCreate()
    {
		$finishedDynamic = false;
		$rootModel = RootApplicant::find()->andWhere(['id' => $_POST['root_id']])->one();
		
		$mouOfficeModel = new MouRegLocation();
		$mouObjectsOfEstablishedModel = new ObjOfEstablishment();
		$authorizedShareModel = new Shares();
		$totalSharesModel = new MouNumberOfShares();
		$mouDateModel = new MouApprovalDate();
		$witnessAddressModel = new WitnessAddress() ;
		$witnessNameModel = new WitnessName();
		$witnessOccupationModel = new WitnessOccupation() ;
		$witnessSignatureModel = new WitnessSignature() ;
		
		$rootModel->application_stage = 3;
		
		// reg location
		
		$mouOfficeModel->root_id = $_POST['root_id'];
		$mouOfficeModel->value = $_POST['moulocation'];
		
		//obj of est
		$mouObjectsOfEstablishedModel->root_id = $_POST['root_id'];
		$mouObjectsOfEstablishedModel->description = $_POST['moudescription'];
		
		// total shares
		$authorizedShareModel->root_id = $_POST['root_id'];
		$authorizedShareModel->value = $_POST['moutotalshares'];
		
		// mou number of shares holders
		$totalSharesModel->root_id = $_POST['root_id'];
		$totalSharesModel->value = $_POST['mounumberofshares'];
		
		$mouDateModel->root_id = $_POST['root_id'];
		$mouDateModel->value= $_POST['mouexpectedstartdate'];
		
		$witnessAddressModel->root_id = $_POST['root_id'];
		$witnessAddressModel->value = $_POST['witnessesaddress'];
		
		$witnessNameModel->root_id = $_POST['root_id'];
		$witnessNameModel->value = $_POST['witnessesname'];
		
		$witnessOccupationModel->root_id = $_POST['root_id'];
		$witnessOccupationModel->value = $_POST['witnessesoccupation'];
		
		$witnessSignatureModel->root_id = $_POST['root_id'];
		$witnessSignatureModel->imageFile = UploadedFile::getInstanceByName('witnessessignature'); 
		
		foreach($_POST['dynamic'] as $key => $value){
			$shareHoldersModel = new ShareHolders();
			$vals = json_decode($value);
			
			$shareHoldersModel->root_id = $_POST['root_id'] ;
			$shareHoldersModel->name = $vals->name;
			$shareHoldersModel->address = $vals->address ;
			$shareHoldersModel->number = $vals->shares;
			$shareHoldersModel->imageFile = UploadedFile::getInstanceByName('dynamicFiles'.$key);
			$shareHoldersModel->upload();
			$finishedDynamic = true;
		}
		
		if(
			$finishedDynamic and 
			$mouOfficeModel->save(false) and 
			$mouObjectsOfEstablishedModel->save(false) and
			$authorizedShareModel->save(false) and
			$totalSharesModel->save(false) and
			$mouDateModel->save(false) and
			$witnessAddressModel->save(false) and
			$witnessNameModel->save(false) and
			$witnessOccupationModel->save(false) and
			$witnessSignatureModel->upload() and
			$rootModel->save(false)  
		){
			Yii::$app->api->sendSuccessResponse($rootModel->attributes);
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
		}
				
		if($importSave and $exportSave and $rootModel->save(false)){
			Yii::$app->api->sendSuccessResponse($rootModel->attributes);
		}else{
			Yii::$app->api->sendFailedResponse('Application Could not be processed');
		}
				
			
			
		
			
		
	}
	
	public function actionGetcompanyname(){
		$rootModel = RootApplicant::find()->andWhere(['id' => $this->request['root_id']])->one();
		
		if(!empty($rootModel) ){
			$data = $rootModel->attributes;
			$data['companyname'] = $rootModel->companyname->value;
			Yii::$app->api->sendSuccessResponse($data);
		}else{
			Yii::$app->api->sendFailedResponse(('Application Could not be processed'));
		}
	} 

 


	
}



